package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.adminconfig.ResponseEliminarZona;
import app.parqueo.model.adminconfig.ResponseInsertarZona;
import app.parqueo.model.adminconfig.Zona;
import app.parqueo.service.MunicipioService;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.ZonaService;

@Controller
public class ZonaController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);
	private final Integer idmenu = 7;
	
	@Autowired
	ZonaService zonaService;
	
	@Autowired
	MunicipioService municipioService;
	
	@Autowired
	PerfilMenuService perfilMenuService;
	
	@RequestMapping("/configuracion/zonasJSON")
	@ResponseBody
	public String listarZonas(
			@RequestParam("idmunicipio") Optional<Integer> idmunicipio,
			@RequestParam("page") Optional<Integer> page, 
			@RequestParam("size") Optional<Integer> size, 
			@RequestParam("activo") Optional<Integer> activo, 
			Model model, HttpSession session) {
		String json = "";
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {
			Integer idmuni = idmunicipio.orElse(0);
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);
			Integer active = activo.orElse(0);
	
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(zonaService.listarZonas(idmuni,currentPage, pageSize,active).getLista());
			} catch (JsonProcessingException e) {
	
			}
		}

		return json;
	}

	@RequestMapping("/configuracion/zonaRegistro")
	public ModelAndView PageInsertarZona(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/zonaRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		Zona zona = new Zona();
		zona.setZona_Activo(true);
		
		modelAndView.addObject("Zona", zona);
		modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());

		return modelAndView;
	}

	@PostMapping("/configuracion/zonaRegistro")
	public ModelAndView insertarZona(@Valid @ModelAttribute("Zona") Zona zona,
			BindingResult bindingResult, Model model, HttpSession session, HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/zona");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();
		
		if(acceso==true) {
			
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
	
	
			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());
			zona.setZona_UsuarioCreacion(id);
	
			if (bindingResult.hasErrors()) {
	
				modelAndView.setViewName("configuracion/zonaRegistro");
				
				modelAndView.addObject("Zona", zona);
				modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
	
				return modelAndView;
			}
	
			try {
	
				ResponseInsertarZona response = this.zonaService.insertarZona(zona);
				modelAndView.addObject("mensaje", null);
	
				if (response.getCodError() != null) {
	
					if (response.getCodError() >= 0 && response.getCodError() <= 2) {
						modelAndView.setViewName("configuracion/zonaRegistro");
						modelAndView.addObject("Zona", zona);	
						modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
						
						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						}else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje","Seleccione la municipalidad");
						}else if (response.getCodError() == 2) {
							modelAndView.addObject("mensaje","La zona ya existe");
						}
	
						return modelAndView;
	
					}
				} else {
					modelAndView.addObject("mensajeInsertar","Insertado");
					modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());

				}
	
			} catch (Exception e) {
				saveError(e, "No se pudo agregar la nueva zona");
			}
		}else {
			modelAndView.setViewName("configuracion/zonaRegistro");
			modelAndView.addObject("Zona", zona);
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}
	
	@GetMapping("/configuracion/zonaRegistro/{id}")
	public ModelAndView PageActualizarZona(@PathVariable Integer id, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/zonaRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
	
		try {									
			modelAndView.addObject("Zona", this.zonaService.seleccionarZona(id).getZona());
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
		} catch (Exception e) {
			modelAndView.addObject("Zona", null);
			saveError(e, "No se encontró la zona con id: " + String.valueOf(id));
		}

		return modelAndView;
	}
	
	@PostMapping("/configuracion/zonaRegistro/{id}")
	public ModelAndView actualizarZona(@Valid @ModelAttribute("Zona") Zona zona,
			BindingResult bindingResult, @PathVariable Integer id, Model model, HttpSession session,
			HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/zona");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();
		
		if(acceso==true) {		
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
	
			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());
	
			if (bindingResult.hasErrors()) {
	
				modelAndView.setViewName("configuracion/zonaRegistro");
				
				modelAndView.addObject("Zona", zona);
				modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
	
				return modelAndView;
			}
	
			try {
				
				zona.setZona_UsuarioEdicion(iduser);
				
				ResponseInsertarZona response = this.zonaService.actualizarZona(zona);
				modelAndView.addObject("mensaje", null);
	
				if (response.getCodError() != null) {
					if (response.getCodError() >= 0 && response.getCodError() <= 1) {
						modelAndView.setViewName("configuracion/zonaRegistro");
						modelAndView.addObject("Zona", zona);
						modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
	
						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "La zona ya existe");
						}
	
						return modelAndView;
	
					}
				} else {
					modelAndView.addObject("mensajeActualizar", "Actualizado");
					modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());

				}
	
			} catch (Exception e) {
				saveError(e, "No se pudo actualizar los datos de la zona");
			}
		}else {
			modelAndView.setViewName("configuracion/zonaRegistro");
			modelAndView.addObject("Zona", zona);
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		return modelAndView;

	}
	
	@RequestMapping(value= "/configuracion/eliminarZona", method= RequestMethod.GET)
	@ResponseBody
	public String eliminarZona(@RequestParam(name="id", required=true) int id, HttpSession session) {
				
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Eliminar();
		
		if(acceso == true) {
			
			try {
				ResponseEliminarZona response = this.zonaService.eliminarZona(id);
	
				if(response.getCodError()!=null) {
					if(response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					}else if (response.getCodError() == 1) {
						return "La zona a eliminar no existe";
					}else if(response.getCodError()==2) {
						return "La zona tiene dependencias en estacionamientos";
					}
				}
		
			}catch(Exception e) {
				saveError(e, "No se pudo eliminar la zona con id: "+String.valueOf(id));
			}
		
		}else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}

	@RequestMapping("/configuracion/zona")
	public ModelAndView pageZona(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/zona");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if(acceso==false) {
			modelAndView.setViewName("error/403");
		}else {
			modelAndView.addObject("Municipios",municipioService.listarMunicipiosCB());
		}
		return modelAndView;
	}

	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}
	
}
