package app.parqueo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.service.AdminReportesService;
import app.parqueo.service.MunicipioService;
import app.parqueo.service.PerfilMenuService;

@Controller
public class AdminReportesController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MunicipioController.class);
	private final Integer idmenu = 11;
	
	@Autowired
	AdminReportesService adminReportesService;

	@Autowired
	PerfilMenuService perfilMenuService;
	
	@Autowired
	MunicipioService municipioService;
	
	@RequestMapping("/parqueo/reporteParqueosZonaJSON")
	@ResponseBody
	public String reporteParqueosZona( 
			@RequestParam("page") Optional<Integer> page, 
		    @RequestParam("size") Optional<Integer> size,
		    @RequestParam("fechainicio") Optional<String> fechainicio,
		    @RequestParam("indiceFechainicio") Optional<Integer> indiceFechainicio,
		    @RequestParam("fechafin") Optional<String> fechafin,
		    @RequestParam("indiceFechaFin") Optional<Integer> indiceFechaFin,
		    @RequestParam("idmunicipio") Optional<Integer> idmunicipio,
		    Model model, HttpSession session) throws ParseException {
		String json = "";
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if (acceso == true) {
			String fechaInicio = fechainicio.orElse(null);
			String fechaFin = fechafin.orElse(null);
			
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);
			Integer indicadorFechainicio = indiceFechainicio.orElse(0);
			Integer indicadorFechaFin = indiceFechaFin.orElse(0);

			Integer idmuni = idmunicipio.orElse(1);
		
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date parsed1 = null, parsed2 = null;
			java.sql.Date fecha1 = null, fecha2 = null;
			 
			if(indicadorFechainicio == 0) {
		        parsed1 = format.parse("2022-08-08");
		        fecha1 = new java.sql.Date(parsed1.getTime());
			}else {	       
		        parsed1 = format.parse(fechaInicio);
		        fecha1 = new java.sql.Date(parsed1.getTime());
	
			}
			
			if(indicadorFechaFin == 0) {
		        parsed2 = format.parse("2022-09-09");
		        fecha2 = new java.sql.Date(parsed2.getTime());
	
			}else {
		        parsed2 = format.parse(fechaFin);
		        fecha2 = new java.sql.Date(parsed2.getTime());
	
			}
	        
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(adminReportesService.reporteDeParqueosZona(currentPage, pageSize,idmuni,fecha1,
						indicadorFechainicio,fecha2,indicadorFechaFin).getLista());
				
			}catch(JsonProcessingException e){

			}
		}
		
		return json;
	}
	
	@RequestMapping("/parqueo/parqueosZona")
	public ModelAndView pageParqueosZona(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("parqueo/parqueosZona");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==false) {
			modelAndView.setViewName("error/403");
		}else {
			modelAndView.addObject("ListaMunicipios",this.municipioService.listarMunicipiosCB());
		}
		return modelAndView;
	}
	
	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}
}
