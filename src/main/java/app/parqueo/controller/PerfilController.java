package app.parqueo.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.adminseguridad.Perfil;
import app.parqueo.model.adminseguridad.PerfilMenu;
import app.parqueo.model.adminseguridad.ResponseInsertarPerfil;
import app.parqueo.model.adminseguridad.ResponsePerfil;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.PerfilService;

@Controller
public class PerfilController {
	private static final Logger LOGGER = LoggerFactory.getLogger(PerfilController.class);
	private final Integer idmenu = 3;
	
	@Autowired
	PerfilService perfilService;
	
	@Autowired
	PerfilMenuService perfilMenuService;
	
	
	@RequestMapping("/seguridad/perfilesJSON")
	@ResponseBody
	public String listarPerfiles( 
			@RequestParam("page") Optional<Integer> page, 
		    @RequestParam("size") Optional<Integer> size, 
		    @RequestParam("nombre") Optional<String> nombre, 
		    @RequestParam("activo") Optional<Integer> activo, 
		    @RequestParam("indiceNombre") Optional<Integer> indiceNombre, 
		    Model model, HttpSession session) {
		String json = "";
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {	
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);	
			String nom = nombre.orElse(null);
			Integer active = activo.orElse(0);
			Integer indicadorNombre = indiceNombre.orElse(0);
	
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(
						perfilService.listarPerfiles(currentPage, pageSize, nom, active, indicadorNombre).getLista());
				
			}catch(JsonProcessingException e){
	
			}
		}
		return json;
	}
	@RequestMapping("/seguridad/perfil")
	public ModelAndView pagePerfil(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/perfil");
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if(acceso==false) {
			modelAndView.setViewName("error/403");
		}
		return modelAndView;
	}
	
	
	@RequestMapping("/seguridad/perfilRegistro")
	public ModelAndView PageInsertarPerfil(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/perfilRegistro");
	
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Crear();
		
		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}

		modelAndView.addObject("ListaConfiguracion", this.perfilMenuService.listarConfiguracionMenuPorPerfil(0).getLista());
		
		
		Perfil perfil = new Perfil();
		perfil.setPerf_Activo(true);
		
		modelAndView.addObject("Perfil",perfil);
			
	    return modelAndView;
	}
	
	@RequestMapping(value= "/seguridad/insertarPerfilMenu", method= RequestMethod.GET)
	public ModelAndView insertarPerfilMenu( @RequestParam("activo") boolean activo, @RequestParam("idmenupadre") Integer idmenupadre,
			@RequestParam("crear") boolean crear, @RequestParam("editar") boolean editar,
			@RequestParam("eliminar") boolean eliminar, @RequestParam("menuId") Integer menuId, @RequestParam("pmenid") Integer pmenid,
			@RequestParam("idPerfil") Integer idPerfil, @RequestParam("insertar") boolean insertar, HttpSession session ) {
		ModelAndView modelAndView = new ModelAndView("seguridad/perfil");
		
		PerfilMenu perfilMenu = new PerfilMenu();
		perfilMenu.setMenu_Id(menuId);
		perfilMenu.setPerf_Id(idPerfil);
		perfilMenu.setPmen_Crear(crear);
		perfilMenu.setPmen_Editar(editar);
		perfilMenu.setPmen_Eliminar(eliminar);
		
		if(idmenupadre==0) {
			perfilMenu.setMenu_PadreId(null);
		}else {
			perfilMenu.setMenu_PadreId(idmenupadre);
		}
		
		Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());

		perfilMenu.setPmen_Id(pmenid);
		perfilMenu.setPmen_Activo(activo);
		perfilMenu.setPmen_UsuarioCreacion(iduser);
		perfilMenu.setPmen_UsuarioEdicion(iduser);
		

		@SuppressWarnings("unchecked")
		List<PerfilMenu> lista = (List<PerfilMenu>) session.getAttribute("listPerfilMenu");
		lista.add(perfilMenu);
		
		session.setAttribute("listPerfilMenu", lista);
		
		if(insertar==true) {
			try {
				for(int i=0; i<lista.size();i++) {
					if(lista.get(i).getPmen_Activo() == false && lista.get(i).getMenu_PadreId()==null) {
						for(int j=0; j<lista.size();j++) {
							if(lista.get(j).getMenu_PadreId()==lista.get(i).getMenu_Id()) {
								lista.get(j).setPmen_Activo(false);
								lista.get(j).setPmen_Crear(false);
								lista.get(j).setPmen_Editar(false);
								lista.get(j).setPmen_Eliminar(false);
							}
						}
					}
				}
				this.perfilMenuService.insertarConfiguracionPerfil(lista);
				modelAndView.addObject("mensajeInsertar","Insertado");
				
				lista.clear();
				session.setAttribute("listPerfilMenu", lista);
			}catch(Exception e) {
				modelAndView.addObject("mensaje",null);
				lista.clear();
				session.setAttribute("listPerfilMenu", lista);
				saveError(e, "No se pudo agregar el nuevo perfil");
			}
			
		}
	
		return modelAndView;
	}
	
	@RequestMapping(value= "/seguridad/insertarPerfil", method= RequestMethod.GET)
	@ResponseBody
	public String insertarPerfil(@RequestParam("nombre") String nombre, @RequestParam("activo") boolean activo,
			HttpSession session) {
		String salida = "";
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Crear();
		
		if(acceso==true) {
			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());
			
			Perfil perfil = new Perfil();
			perfil.setPerf_Id(0);
			perfil.setPerf_UsuarioCreacion(id);
			perfil.setPerf_Nombre(nombre);
			perfil.setPerf_Activo(activo);
			
			try {
				ResponseInsertarPerfil response = this.perfilService.insertarPerfil(perfil);
				
				if(response.getCodError()!=null) {
					if(response.getCodError() == 0) {
						salida = null;
					}else if(response.getCodError() == 1) {
						salida = "0";
					}
				}else {
					salida = ""+response.getPerf_Id();
				}
	
			}catch(Exception e) {
				salida = null;
			}
		}
		return salida;
	}
	
	@GetMapping("/seguridad/perfilRegistro/{id}")
	public ModelAndView PageActualizarPerfil(@PathVariable Integer id, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/perfilRegistro");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Editar();
		
		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}

		try {
			modelAndView.addObject("ListaConfiguracion", this.perfilMenuService.listarConfiguracionMenuPorPerfil(id).getLista());
		    modelAndView.addObject("Perfil", this.perfilService.seleccionarPerfil(id).getPerfil());
		    
		}catch (Exception e) {
			modelAndView.addObject("Perfil",null);
			saveError(e, "No se encontró al perfil con id: "+String.valueOf(id));
		}
	
		return modelAndView;
	}
	
	@RequestMapping(value= "/seguridad/actualizarPerfil", method= RequestMethod.GET)
	@ResponseBody
	public String actualizarPerfil(@RequestParam("nombre") String nombre, @RequestParam("activo") boolean activo,
			 @RequestParam("id") Integer id, HttpSession session) {
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Editar();
		String salida = "";
		
		if(acceso==true) {
			
			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());
			
			Perfil perfil = new Perfil();
			perfil.setPerf_Id(id);
			perfil.setPerf_Nombre(nombre);
			perfil.setPerf_Activo(activo);
			perfil.setPerf_UsuarioEdicion(iduser);
	
			try {
				ResponsePerfil response = this.perfilService.actualizarPerfil(perfil);
				
				if(response.getCodError()!=null) {
					if(response.getCodError() == 0) {
						salida = null;
					}else if(response.getCodError() == 1) {
						salida = "0";
					}
				}else {
					salida = ""+id;
				}
	
			}catch(Exception e) {
				salida = null;
			}
		}
				
		return salida;
		
	}
	

	@RequestMapping(value= "/seguridad/eliminarPerfil", method= RequestMethod.GET)
	@ResponseBody
	public String eliminarPerfil(@RequestParam(name="id", required=true) int id, HttpSession session) {
				
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Eliminar();
		
		if(acceso == true) {
			
			try {
				ResponsePerfil response = this.perfilService.eliminarPerfil(id);
	
				if(response.getCodError()!=null) {
					if(response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					}else if (response.getCodError() == 1) {
						return "El perfil no esta registrado";
					}else if(response.getCodError() == 2) {
						return "El perfil tiene dependencias";
					}
				}
		
			}catch(Exception e) {
				saveError(e, "No se pudo eliminar el perfil con id: "+String.valueOf(id));
			}
		
		}else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}

	
	public void saveError(Exception e, String informacion) {
		LOGGER.error("\n***************************************************************************************************"
				+ "\nInformación: "+ informacion
				+ "\nMensaje: "+e.getMessage()
				+"\nCausa: "+e.getCause()+"\n\n");
	}
}
