package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.service.ClienteService;
import app.parqueo.service.PerfilMenuService;

@Controller
public class ClienteController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MunicipioController.class);
	private final Integer idmenu = 9;

	@Autowired
	ClienteService clienteService;
	
	@Autowired
	PerfilMenuService perfilMenuService;
	
	@RequestMapping("/maestros/clientesJSON")
	@ResponseBody
	public String listarClientes( 
			@RequestParam("page") Optional<Integer> page, 
		    @RequestParam("size") Optional<Integer> size, 
		    @RequestParam("nombre") Optional<String> nombre, 
		    @RequestParam("indiceNombre") Optional<Integer> indiceNombre, 
		    @RequestParam("nrodocumento") Optional<String> nrodocumento, 
		    @RequestParam("indiceNrodocumento") Optional<Integer> indiceNrodocumento, 
		    @RequestParam("placa") Optional<String> placa, 
		    @RequestParam("indicePlaca") Optional<Integer> indicePlaca, 
		    Model model, HttpSession session
			) {
		String json = "";
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);	
			String nom = nombre.orElse(null);
			String numeroDocumento = nrodocumento.orElse(null);
			String placaVehiculo = placa.orElse(null);
			Integer indicadorNombre = indiceNombre.orElse(0);
			Integer indicadorNrodocumento = indiceNrodocumento.orElse(0);
			Integer indicacorPlaca = indicePlaca.orElse(0);
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(clienteService.listarClientes(currentPage, pageSize, nom,indicadorNombre,
						numeroDocumento, indicadorNrodocumento, placaVehiculo, indicacorPlaca).getLista());
				
			}catch(JsonProcessingException e){
	
			}	
		}
		return json;
	}
	
	@RequestMapping("/parqueo/cliente")
	public ModelAndView pageCliente(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("parqueo/cliente");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if(acceso==false) {
			modelAndView.setViewName("error/403");
		}
		return modelAndView;
	}

	
	@RequestMapping("/parqueo/clienteDetalleJSON")
	@ResponseBody
	public String DetalleCliente(@RequestParam Integer id) {
		String json = "";		
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString( this.clienteService.seleccionarDetalleCliente(id).getDetalleCliente());
			
		}catch (Exception e) {
		
		}

		return json;
	}
	
	@RequestMapping("/parqueo/clienteVehiculosJSON")
	@ResponseBody
	public String ClienteVehiculos(@RequestParam Integer id) {
		String json = "";
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(this.clienteService.listarVehiculosPorCliente(id).getVehiculos());

		}catch (Exception e) {
		
		}

		return json;
	}
	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}
}
