package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.adminseguridad.ResponseRecuperarContraseniaAdmin;
import app.parqueo.model.adminseguridad.ResponseSeleccionarUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuario;
import app.parqueo.model.adminseguridad.Usuario;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.PerfilService;
import app.parqueo.service.UsuarioService;

@Controller
public class UsuarioController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);
	private final Integer idmenu = 2;
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	PerfilService perfilService;
	
	@Autowired
	PerfilMenuService perfilMenuService;
	
	@RequestMapping("/seguridad/usuariosFiltrosJSON")
	@ResponseBody
	public String listarUsuarios( @RequestParam("page") Optional<Integer> page, 
		      @RequestParam("size") Optional<Integer> size, @RequestParam("nombre") Optional<String> nombre,
		      @RequestParam("idperfil") Optional<Integer> idperfil, @RequestParam("activo") Optional<Integer> activo,
		      @RequestParam("indicadorNombre") Optional<Integer> indicadorNombre, Model model,HttpSession session) {
		String json = "";
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);		
			String nom = nombre.orElse(null);
			Integer active = activo.orElse(0);
			Integer perf_Id = idperfil.orElse(0);
			Integer indice = indicadorNombre.orElse(0); 
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(usuarioService.listarUsuarios(currentPage, pageSize,nom,perf_Id,active, indice).getLista());
				
			}catch(JsonProcessingException e){
	
			}
		}

		return json;
	}
	
	@RequestMapping("/seguridad/usuario")
	public ModelAndView pageUsuario(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/usuario");
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if(acceso==false) {
			modelAndView.setViewName("error/403");
		}else {
			try {
				modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
			}catch (Exception e) {
				modelAndView.addObject("listaperfiles",null);
				saveError(e, "No se pudo listar los perfiles del combobox");
			}	
		}
		return modelAndView;
	}
	
	@RequestMapping("/seguridad/usuarioRegistro")
	public ModelAndView PageInsertarUsuario(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/usuarioRegistro");
	
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Crear();
		
		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		
		Usuario usuario = new Usuario();
		usuario.setUsua_Activo(true);
		
		modelAndView.addObject("Usuario",usuario);
		try {
			modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
		}catch (Exception e) {
			modelAndView.addObject("listaperfiles",null);
			saveError(e, "No se pudo listar los perfiles del combobox");
		}	
		
			
	    return modelAndView;
	}
	
	@PostMapping("/seguridad/usuarioRegistro")
	public ModelAndView insertarUsuario(@Valid @ModelAttribute("Usuario") Usuario usuario,
			BindingResult bindingResult, Model model,HttpSession session, HttpServletRequest request) {
				
		ModelAndView modelAndView = new ModelAndView("seguridad/usuario");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Crear();
		
		if(acceso==true) {
			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());
					
			usuario.setUsua_UsuarioCreacion(id);	
			
			if (bindingResult.hasErrors()) {
				
				
				modelAndView.setViewName("seguridad/usuarioRegistro");
				
				try {
					modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
	
				}catch (Exception e) {
					modelAndView.addObject("listaperfiles",null);
					saveError(e, "No se pudo listar los perfiles del combobox");
				}
				
				modelAndView.addObject("Usuario", usuario);
				
				return modelAndView;
		    }
			
			try {
				
				ResponseUsuario response = usuarioService.insertarUsuario(usuario);
				modelAndView.addObject("mensaje",null);
				
				if(response.getCodError() !=null) {
	
					if(response.getCodError()>=0 && response.getCodError()<=3 ) {
						modelAndView.setViewName("seguridad/usuarioRegistro");
						modelAndView.addObject("Usuario", usuario);
		
						try {
							modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
		
						}catch (Exception e) {
							modelAndView.addObject("listaperfiles",null);
							saveError(e, "No se pudo listar los perfiles del combobox");
						}
						
							if(response.getCodError() == 0) {
								modelAndView.addObject("mensaje","Se ha producido un error, Intentelo más tarde");
							}else if (response.getCodError() == 2) {
								modelAndView.addObject("mensaje","El Alias ya existe");
							}else if (response.getCodError() == 3) {
								modelAndView.addObject("mensaje","El Correo ya existe");
							}
										
						return modelAndView;
						
					}
				}else {
					modelAndView.addObject("mensajeInsertar","Insertado");
					try {
						modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
					}catch (Exception e) {
						modelAndView.addObject("listaperfiles",null);
						saveError(e, "No se pudo listar los perfiles del combobox");
					}	
				}
				
			}catch(Exception e) {
				saveError(e, "No se pudo agregar al nuevo usuario");
			}
		}else {
			modelAndView.setViewName("seguridad/usuarioRegistro");
			modelAndView.addObject("Usuario", usuario);
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		
		return modelAndView;
		
	}
	

	@GetMapping("/seguridad/usuarioRegistro/{id}")
	public ModelAndView PageActualizarUsuario(@PathVariable Integer id, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("seguridad/usuarioRegistro");	
			
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Editar();
		
		if(acceso == false) {
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		
		try {
		    modelAndView.addObject("Usuario", this.usuarioService.seleccionarUsuario(id));

		}catch (Exception e) {
			modelAndView.addObject("Usuario",null);
			saveError(e, "No se pudo seleccionar los datos del cliente");
		}
	    
	    try {
			modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());

		}catch (Exception e) {
			modelAndView.addObject("listaperfiles",null);
			saveError(e, "No se pudo listar los perfiles del combobox");
		}

		return modelAndView;
	}
	
	
	@PostMapping("/seguridad/usuarioRegistro/{id}")
	public ModelAndView actualizarUsuario(@Valid @ModelAttribute("Usuario") Usuario usuario,BindingResult bindingResult,
			@PathVariable Integer id, Model model, HttpSession session,  HttpServletRequest request) {
		
		ModelAndView modelAndView = new ModelAndView("seguridad/usuario");
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Editar();
		
		if(acceso == true) {
			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());
			usuario.setUsua_UsuarioEdicion(iduser);	
					
			if (bindingResult.hasErrors()) {
							
				modelAndView.setViewName("seguridad/usuarioRegistro");
				try {
					modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
	
				}catch (Exception e) {
					modelAndView.addObject("listaperfiles",null);
					saveError(e, "No se pudo listar los perfiles del combobox");
				}
				modelAndView.addObject("Usuario", usuario);
				
				return modelAndView;
		    }
			
	
			try {
				ResponseUsuario response = usuarioService.actualizarUsuario(usuario);
				modelAndView.addObject("mensaje",null);
				
				if (response.getCodError() != null) {		
					if(response.getCodError()>=0 && response.getCodError()<=1) {
						modelAndView.setViewName("seguridad/usuarioRegistro");
						modelAndView.addObject("Usuario", usuario);
		
						try {
							modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
		
						}catch (Exception e) {
							modelAndView.addObject("listaperfiles",null);
							saveError(e, "No se pudo listar los perfiles del combobox");
							
						}
						
						if(response.getCodError() == 0) {
							modelAndView.addObject("mensaje","Se ha producido un error, Intentelo más tarde");
						}else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje","El Correo ya existe");
						}
					
						return modelAndView;
						
					}
				}else {
				
					modelAndView.addObject("mensajeActualizar","Actualizado");
					try {
						modelAndView.addObject("listaperfiles", this.perfilService.listarPerfilesCB());
					}catch (Exception e) {
						modelAndView.addObject("listaperfiles",null);
						saveError(e, "No se pudo listar los perfiles del combobox");
					}
				}
				
			}catch(Exception e) {
				saveError(e, "No se pudo actualizar los datos del usuario");
			}
				
		}else {
			modelAndView.setViewName("seguridad/usuarioRegistro");
			modelAndView.addObject("Usuario", usuario);
			modelAndView.addObject("mensaje","Usted no tiene los accesos necesarios para este recurso");
		}
		
		return modelAndView;
	}
	

	/*@RequestMapping(value= "/seguridad/eliminarUsuario", method= RequestMethod.GET)
	@ResponseBody
	public String eliminarUsuario(@RequestParam(name="id", required=true) int id, HttpSession session) {
				
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu().getPmen_Eliminar();
		
		if(acceso == true) {
			
			try {
				ResponseUsuario response = this.usuarioService.eliminarUsuario(id);
	
				if(response.getCodError()!=null) {
					if(response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					}else if (response.getCodError() == 1) {
						return "El usuario a eliminar no existe";
					}
				}
		
			}catch(Exception e) {
				saveError(e, "No se pudo eliminar al usuario con id: "+String.valueOf(id));
			}
		
		}else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}*/
	
	@RequestMapping(value = "/seguridad/ReenviarCredencialesUsuario", method = RequestMethod.GET)
	@ResponseBody
	public String reenviarCredencialesUsuario(@RequestParam(name = "id", required = true) int id,
			HttpSession session) {

		try {
			ResponseRecuperarContraseniaAdmin response = this.usuarioService.reenviarCredencialesUsuario(id);

			if (response.getCodError() != null) {
				if (response.getCodError() == 0) {
					return "Se ha producido un error, Intentelo más tarde";
				} else if (response.getCodError() == 1) {
					return "El usuario a eliminar no existe";
				}
			}

			return "";

		} catch (Exception e) {
			saveError(e, "No se pudo reenviar las credenciales al usuario con id: " + String.valueOf(id));
			return "Error";
		}
	}
	
	@RequestMapping("/login")
	public String login() {
	    return "login";
	}
	
	@GetMapping("/")
	public String index() {
		return "index";
	}
	
	@GetMapping("/recuperacion")
	public String recuperacion() {
		return "recuperacion";
	}

	@RequestMapping(value = "/recuperar/RecuperarCredencialesUsuario", method = RequestMethod.GET)
	@ResponseBody
	public String recuperarCredencialesUsuario(@RequestParam(name = "correo", required = true) String correo) {
		String json = "";
		try {

			ResponseSeleccionarUsuario response = this.usuarioService.seleccionarUsuarioCorreo(correo);
			
			if(response.getCodError() !=null) {
				if(response.getCodError() == 0) {
					json = "Error del servidor, inténtalo más tarde";
				}else if(response.getCodError() == 1) {
					json = "Correo no registrado";
				}
			}else {
				this.usuarioService.reenviarCredencialesUsuario(response.getUsuario().getUsua_Id());
				
			}

		} catch (Exception e) {
			saveError(e, "No se pudo recuperar las credenciales al usuario con correo: " + correo);
			json =  "Error del servidor, inténtalo más tarde";
		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(json);
			
		}catch(JsonProcessingException e){

		}
		return json;
	}

	public void saveError(Exception e, String informacion) {
		LOGGER.error("\n***************************************************************************************************"
				+ "\nInformación: "+ informacion
				+ "\nMensaje: "+e.getMessage()
				+"\nCausa: "+e.getCause()+"\n\n");
	}
}
