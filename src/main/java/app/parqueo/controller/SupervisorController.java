package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.adminmaestros.ResponseReenviarCredencialesSupervisor;
import app.parqueo.model.adminmaestros.ResponseSupervisor;
import app.parqueo.model.adminmaestros.Supervisor;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.SupervisorService;
import app.parqueo.service.TipoDocumentoService;

@Controller
public class SupervisorController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SupervisorController.class);
	private final Integer idmenu = 5;

	@Autowired
	SupervisorService supervisorService;

	@Autowired
	PerfilMenuService perfilMenuService;

	@Autowired
	TipoDocumentoService tipoDocumentoService;

	@RequestMapping("/configuracion/supervisoresJSON")
	@ResponseBody
	public String listarSupervisores(@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size, @RequestParam("nombre") Optional<String> nombre,
			@RequestParam("documento") Optional<String> documento, @RequestParam("activo") Optional<Integer> activo,
			Model model,HttpSession session) {
		String json = "";
		
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {
	
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);
			String nom = nombre.orElse("*");
			String doc = documento.orElse("*");
			Integer active = activo.orElse(0);
	
			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(
						supervisorService.listarSupervisores(currentPage, pageSize, nom, doc, active).getLista());
	
			} catch (JsonProcessingException e) {
	
			}
		
		}
		return json;
	}

	@RequestMapping("/configuracion/supervisor")
	public ModelAndView pageSupervisor(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/supervisor");
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if (acceso == false) {
			modelAndView.setViewName("error/403");
		}
		return modelAndView;
	}

	@RequestMapping("/configuracion/supervisorRegistro")
	public ModelAndView PageInsertarSupervisor(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/supervisorRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		Supervisor supervisor = new Supervisor();
		supervisor.setSupe_Activo(true);
		modelAndView.addObject("Supervisor", supervisor);

		try {
			modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());
		} catch (Exception e) {
			modelAndView.addObject("listatipodocumento", null);
			saveError(e, "No se pudo listar los tipo de documentos del combobox");
		}

		return modelAndView;
	}

	@PostMapping("/configuracion/supervisorRegistro")
	public ModelAndView insertarSupervisor(@Valid @ModelAttribute("Supervisor") Supervisor supervisor,
			BindingResult bindingResult, Model model, HttpSession session, HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/supervisor");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());
		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == true) {
			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());
			supervisor.setSupe_UsuarioCreacion(id);

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/supervisorRegistro");

				try {
					modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());

				} catch (Exception e) {
					modelAndView.addObject("listatipodocumento", null);
					saveError(e, "No se pudo listar los tipo de documentos del combobox");
				}

				modelAndView.addObject("Supervisor", supervisor);

				return modelAndView;
			}

			try {

				ResponseSupervisor response = supervisorService.insertarSupervisor(supervisor);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {

					if (response.getCodError() >= 0 && response.getCodError() <= 3) {
						modelAndView.setViewName("configuracion/supervisorRegistro");
						modelAndView.addObject("Supervisor", supervisor);

						try {
							modelAndView.addObject("listatipodocumento",
									this.tipoDocumentoService.listarTipoDocumentos());

						} catch (Exception e) {
							modelAndView.addObject("listatipodocumento", null);
							saveError(e, "No se pudo listar los tipo de documentos del combobox");
						}

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "El alias ya existe");
						} else if (response.getCodError() == 2) {
							modelAndView.addObject("mensaje", "El correo ya está registrado");
						} else if (response.getCodError() == 3) {
							modelAndView.addObject("mensaje", "El número de documento ya está registrado");
						}

						return modelAndView;

					}
				} else {
					modelAndView.addObject("mensajeInsertar", "Insertado");
				}

			} catch (Exception e) {
				saveError(e, "No se pudo agregar al nuevo supervisor");
			}
		} else {
			modelAndView.setViewName("configuracion/supervisorRegistro");

			try {
				modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());

			} catch (Exception e) {
				modelAndView.addObject("listatipodocumento", null);
				saveError(e, "No se pudo listar los tipo de documentos del combobox");
			}

			modelAndView.addObject("Supervisor", supervisor);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}

	@GetMapping("/configuracion/supervisorRegistro/{id}")
	public ModelAndView PageActualizarSupervisor(@PathVariable Integer id, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/supervisorRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		try {
			modelAndView.addObject("Supervisor", this.supervisorService.seleccionarSupervisor(id));

		} catch (Exception e) {
			modelAndView.addObject("Supervisor", null);
			saveError(e, "No se encontró al supervisor con id: " + String.valueOf(id));
		}

		try {
			modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());
		} catch (Exception e) {
			modelAndView.addObject("listatipodocumento", null);
			saveError(e, "No se pudo listar los tipo de documentos del combobox");
		}

		return modelAndView;
	}

	@PostMapping("/configuracion/supervisorRegistro/{id}")
	public ModelAndView actualizarSupervisor(@Valid @ModelAttribute("Supervisor") Supervisor supervisor,
			BindingResult bindingResult, @PathVariable Integer id, Model model, HttpSession session,
			HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/supervisor");
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == true) {
			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());
			supervisor.setSupe_UsuarioEdicion(iduser);

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/supervisorRegistro");

				try {
					modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());

				} catch (Exception e) {
					modelAndView.addObject("listatipodocumento", null);
					saveError(e, "No se pudo listar los tipo de documentos del combobox");
				}

				modelAndView.addObject("Supervisor", supervisor);

				return modelAndView;
			}

			try {
				ResponseSupervisor response = supervisorService.actualizarSupervisor(supervisor);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {
					if (response.getCodError() >= 0 && response.getCodError() <= 2) {
						modelAndView.setViewName("configuracion/supervisorRegistro");
						modelAndView.addObject("Supervisor", supervisor);

						try {
							modelAndView.addObject("listatipodocumento",
									this.tipoDocumentoService.listarTipoDocumentos());

						} catch (Exception e) {
							modelAndView.addObject("listatipodocumento", null);
							saveError(e, "No se pudo listar los tipo de documentos del combobox");
						}

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "El correo ya está registrado");
						} else if (response.getCodError() == 2) {
							modelAndView.addObject("mensaje", "El número de documento ya está registrado");
						}

						return modelAndView;

					}
				} else {
					modelAndView.addObject("mensajeActualizar", "Actualizado");
				}

			} catch (Exception e) {
				saveError(e, "No se pudo actualizar los datos del supervisor");
			}
		} else {
			modelAndView.setViewName("configuracion/supervisorRegistro");

			try {
				modelAndView.addObject("listatipodocumento", this.tipoDocumentoService.listarTipoDocumentos());

			} catch (Exception e) {
				modelAndView.addObject("listatipodocumento", null);
				saveError(e, "No se pudo listar los tipo de documentos del combobox");
			}

			modelAndView.addObject("Supervisor", supervisor);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}

	/*
	@RequestMapping(value = "/configuracion/eliminarSupervisor", method = RequestMethod.GET)
	@ResponseBody
	public String eliminarSupervisor(@RequestParam(name = "id", required = true) int id, HttpSession session) {

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Eliminar();

		if (acceso == true) {

			try {
				ResponseSupervisor response = this.supervisorService.eliminarSupervisor(id);

				if (response.getCodError() != null) {
					if (response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					} else if (response.getCodError() == 1) {
						return "El supervisor a eliminar no existe";
					}
				}

			} catch (Exception e) {
				saveError(e, "No se pudo eliminar al supervisor con id: " + String.valueOf(id));
			}

		} else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}*/

	@RequestMapping(value = "/configuracion/ReenviarCredencialesSupervisor", method = RequestMethod.GET)
	@ResponseBody
	public String reenviarCredencialesSupervisor(@RequestParam(name = "id", required = true) int id,
			HttpSession session) {

		try {
			ResponseReenviarCredencialesSupervisor response = this.supervisorService.reenviarCredencialesSupervisor(id);

			if (response.getCodError() != null) {
				if (response.getCodError() == 0) {
					return "Se ha producido un error, Intentelo más tarde";
				} else if (response.getCodError() == 1) {
					return "El supervisor a eliminar no existe";
				}
			}

			return "";

		} catch (Exception e) {
			saveError(e, "No se pudo reenviar las credenciales al supervisor con id: " + String.valueOf(id));
			return "Error";
		}
	}

	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}
}
