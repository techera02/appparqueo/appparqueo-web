package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.adminconfig.ResponseActualizarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseEliminarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseInsertarEstacionamiento;
import app.parqueo.model.mapa.Estacionamiento;
import app.parqueo.service.EstacionamientoService;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.ZonaService;

@Controller
public class EstacionamientoController {

	@Autowired
	PerfilMenuService perfilMenuService;
	
	@Autowired
	ZonaService zonaService;

	@Autowired
	EstacionamientoService estacionamientoService;

	private static final Logger LOGGER = LoggerFactory.getLogger(MunicipioController.class);

	private final Integer idmenu = 7;

	@RequestMapping("/configuracion/estacionamientosJSON")
	@ResponseBody
	public String listarEstacionamientos(@RequestParam("idZona") Integer idZona,
			@RequestParam("page") Optional<Integer> page, @RequestParam("size") Optional<Integer> size, 
			@RequestParam("nombre") Optional<String> nombre, @RequestParam("indicenombre") Optional<Integer> indicenombre,
			@RequestParam("codigo") Optional<String> codigo, @RequestParam("indicecodigo") Optional<Integer> indicecodigo,
			Model model, HttpSession session) {
		String json = "";
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		
		if(acceso==true) {
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);
			String nom = nombre.orElse("null");
			Integer indicadornombre = indicenombre.orElse(0);
			String cod = codigo.orElse("null");
			Integer indicadorcodigo = indicecodigo.orElse(0);
	
			ObjectMapper mapper = new ObjectMapper();
	
			try {
				json = mapper.writeValueAsString(
						estacionamientoService.listarEstacionamientosPorZona(idZona, currentPage, pageSize,
								nom,indicadornombre,cod, indicadorcodigo).getLista());
	
			} catch (JsonProcessingException e) {
	
			}
		}

		return json;
	}

	@RequestMapping("/configuracion/estacionamiento/{idZona}")
	public ModelAndView pageEstacionamientos(@PathVariable Integer idZona, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/estacionamiento");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();

		if (acceso == false) {
			modelAndView.setViewName("error/403");
		} else {
			modelAndView.addObject("idZona", idZona);
			modelAndView.addObject("nombreZona", this.zonaService.seleccionarZona(idZona).getZona().getZona_Descripcion());
		}

		return modelAndView;
	}

	@RequestMapping("/configuracion/estacionamientoRegistro/{idZona}")
	public ModelAndView PageInsertarEstacionamiento(@PathVariable Integer idZona, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/estacionamientoRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}
		
		Estacionamiento estacionamiento = new Estacionamiento();
		estacionamiento.setEsta_Disponible(true);
		modelAndView.addObject("Estacionamiento", estacionamiento);

		return modelAndView;
	}

	@PostMapping("/configuracion/estacionamientoRegistro/{idZona}")
	public ModelAndView insertarEstacionamiento(
			@Valid @ModelAttribute("Estacionamiento") Estacionamiento estacionamiento, BindingResult bindingResult,
			Model model, HttpSession session, HttpServletRequest request, @PathVariable Integer idZona) {

		ModelAndView modelAndView = new ModelAndView("redirect:/configuracion/estacionamiento/" + idZona);

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == true) {

			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());

			estacionamiento.setEsta_UsuarioCreacion(id);

			estacionamiento.setZona_Id(idZona);

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/estacionamientoRegistro");

				modelAndView.addObject("Estacionamiento", estacionamiento);

				return modelAndView;
			}

			try {

				ResponseInsertarEstacionamiento response = this.estacionamientoService
						.insertarEstacionamiento(estacionamiento);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {

					if (response.getCodError() >= 0 && response.getCodError() <= 3) {
						modelAndView.setViewName("configuracion/estacionamientoRegistro");
						modelAndView.addObject("Estacionamiento", estacionamiento);

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 2) {
							modelAndView.addObject("mensaje", "El estacionamiento ya existe");
						} else if(response.getCodError() == 3) {
							modelAndView.addObject("mensaje", "El codigo estacionamiento ya existe");
						}

						return modelAndView;

					}
				}

			} catch (Exception e) {
				saveError(e, "No se pudo agregar al nuevo estacionamiento");
			}

		} else {
			modelAndView.setViewName("configuracion/estacionamientoRegistro");
			modelAndView.addObject("Estacionamiento", estacionamiento);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}

	@GetMapping("/configuracion/estacionamientoRegistros/{idEsta}")
	public ModelAndView PageActualizarEstacionamiento(@PathVariable Integer idEsta, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/estacionamientoRegistro");

		Estacionamiento estacionamiento = this.estacionamientoService.seleccionarEstacionamiento(idEsta)
				.getEstacionamiento();

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		try {
			modelAndView.addObject("Estacionamiento", estacionamiento);

		} catch (Exception e) {
			modelAndView.addObject("Estacionamiento", null);
			saveError(e, "No se pudo seleccionar los datos del estacionamiento");
		}

		return modelAndView;
	}

	@PostMapping("/configuracion/estacionamientoRegistros/{idEsta}")
	public ModelAndView actualizarEstacionamiento(
			@Valid @ModelAttribute("Estacionamiento") Estacionamiento estacionamiento, BindingResult bindingResult,
			@PathVariable Integer idEsta, Model model, HttpSession session, HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView(
				"redirect:/configuracion/estacionamiento/" + estacionamiento.getZona_Id());

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == true) {

			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());
			estacionamiento.setEsta_UsuarioEdicion(iduser);

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/estacionamientoRegistro");

				modelAndView.addObject("Estacionamiento", estacionamiento);

				return modelAndView;
			}

			try {
				ResponseActualizarEstacionamiento response = this.estacionamientoService
						.actualizarEstacionamiento(estacionamiento);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {
					if (response.getCodError() >= 0 && response.getCodError() <= 3) {
						modelAndView.setViewName("configuracion/estacionamientoRegistro");
						modelAndView.addObject("Estacionamiento", estacionamiento);

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "El estacionamiento  ya existe");
						} else if(response.getCodError() == 3) {
							modelAndView.addObject("mensaje", "El codigo estacionamiento ya existe");
						}

						return modelAndView;

					}
				}

			} catch (Exception e) {
				saveError(e, "No se pudo actualizar los datos del estacionamiento");
			}

		} else {
			modelAndView.setViewName("configuracion/estacionamientoRegistro");
			modelAndView.addObject("Estacionamiento", estacionamiento);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}

	@RequestMapping(value = "/configuracion/eliminarEstacionamiento", method = RequestMethod.GET)
	@ResponseBody
	public String eliminarUsuario(@RequestParam(name = "id", required = true) int id, HttpSession session) {

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Eliminar();

		if (acceso == true) {

			try {
				ResponseEliminarEstacionamiento response = this.estacionamientoService.eliminarEstacionamiento(id);

				if (response.getCodError() != null) {
					if (response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					} else if (response.getCodError() == 1) {
						return "Estacionamiento no registrado";
					}
				}

			} catch (Exception e) {
				saveError(e, "No se pudo eliminar el estacionamiento con id: " + String.valueOf(id));
			}

		} else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}

	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}

}
