package app.parqueo.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import app.parqueo.model.MunicipioTarifa;
import app.parqueo.model.adminconfig.ResponseTarifa;
import app.parqueo.model.adminconfig.Tarifa;
import app.parqueo.model.adminmaestros.Municipio;
import app.parqueo.model.adminmaestros.ResponseMunicipio;
import app.parqueo.service.MunicipioService;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.TarifaService;

@Controller
public class MunicipioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MunicipioController.class);
	private final Integer idmenu = 6;

	@Autowired
	MunicipioService municipioService;

	@Autowired
	PerfilMenuService perfilMenuService;

	@Autowired
	TarifaService tarifaService;

	@RequestMapping("/configuracion/municipiosJSON")
	@ResponseBody
	public String listarMunicipiosFiltros(@RequestParam("page") Optional<Integer> page,
			@RequestParam("size") Optional<Integer> size, @RequestParam("nombre") Optional<String> nombre,
			@RequestParam("activo") Optional<Integer> activo, Model model, HttpSession session) {
		String json = "";
		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();

		if (acceso == true) {
			Integer currentPage = page.orElse(1);
			Integer pageSize = size.orElse(5);
			String nom = nombre.orElse("*");
			Integer active = activo.orElse(0);

			ObjectMapper mapper = new ObjectMapper();
			try {
				json = mapper.writeValueAsString(
						municipioService.listarMunicipios(currentPage, pageSize, nom, active).getLista());

			} catch (JsonProcessingException e) {

			}
		}
		return json;
	}

	@RequestMapping("/configuracion/municipio")
	public ModelAndView pageMunicipio(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/municipio");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Activo();
		if (acceso == false) {
			modelAndView.setViewName("error/403");
		}

		return modelAndView;
	}

	@RequestMapping("/configuracion/municipioRegistro")
	public ModelAndView PageInsertarMunicipio(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/municipioRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		MunicipioTarifa municipioTarifa = new MunicipioTarifa();
		municipioTarifa.setMuni_Activo(true);

		modelAndView.addObject("MunicipioTarifa", municipioTarifa);

		return modelAndView;
	}

	@PostMapping("/configuracion/municipioRegistro")
	public ModelAndView insertarMunicipio(@Valid @ModelAttribute("MunicipioTarifa") MunicipioTarifa municipioTarifa,
			BindingResult bindingResult, Model model, HttpSession session, HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/municipio");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Crear();

		if (acceso == true) {

			Integer id = Integer.parseInt(session.getAttribute("idusuario").toString());

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/municipioRegistro");
				modelAndView.addObject("MunicipioTarifa", municipioTarifa);

				return modelAndView;
			}

			try {

				Municipio municipio = new Municipio();
				municipio.setMuni_Nombre(municipioTarifa.getMuni_Nombre());
				municipio.setMuni_Activo(municipioTarifa.getMuni_Activo());
				municipio.setMuni_UsuarioCreacion(id);

				ResponseMunicipio response = municipioService.insertarMunicipio(municipio);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {

					if (response.getCodError() >= 0 && response.getCodError() <= 1) {
						modelAndView.setViewName("configuracion/municipioRegistro");
						modelAndView.addObject("MunicipioTarifa", municipioTarifa);

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "El nombre ya está registrado");
						}

						return modelAndView;
					}

				} else {

					Tarifa tarifa = new Tarifa();
					tarifa.setMuni_Id(response.getMuni_Id());
					tarifa.setTari_Monto(municipioTarifa.getTari_Monto());
					tarifa.setTari_UsuarioCreacion(id);

					ResponseTarifa response2 = tarifaService.insertarTarifa(tarifa);

					if (response2.getCodError() != null) {

						if (response2.getCodError() >= 0 && response2.getCodError() <= 2) {
							modelAndView.setViewName("configuracion/municipioRegistro");
							modelAndView.addObject("MunicipioTarifa", municipioTarifa);

							if (response2.getCodError() == 0) {
								modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
							} else if (response2.getCodError() == 1) {
								modelAndView.addObject("mensaje", "Municipalidad vacio");
							} else if (response2.getCodError() == 2) {
								modelAndView.addObject("mensaje", "La tarifa ya existe");
							}

							return modelAndView;

						}

					} else {
						modelAndView.addObject("mensajeInsertar", "Insertado");
					}

				}

			} catch (Exception e) {
				saveError(e, "No se pudo insertar los datos del municipio");
			}
		} else {
			modelAndView.setViewName("configuracion/municipioRegistro");
			modelAndView.addObject("MunicipioTarifa", municipioTarifa);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}

		return modelAndView;

	}

	@GetMapping("/configuracion/municipioRegistro/{id}")
	public ModelAndView PageActualizarMunicipio(@PathVariable Integer id, HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("configuracion/municipioRegistro");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == false) {
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}
		try {

			Municipio municipio = this.municipioService.seleccionarMunicipio(id);
			Tarifa tarifa = this.tarifaService.seleccionarTarifaPorMunicipio(id);

			MunicipioTarifa municipioTarifa = new MunicipioTarifa();
			municipioTarifa.setMuni_Id(municipio.getMuni_Id());
			municipioTarifa.setMuni_Nombre(municipio.getMuni_Nombre());
			municipioTarifa.setMuni_Activo(municipio.getMuni_Activo());

			municipioTarifa.setTari_Id(tarifa.getTari_Id());
			municipioTarifa.setTari_Monto(tarifa.getTari_Monto());

			modelAndView.addObject("MunicipioTarifa", municipioTarifa);

		} catch (Exception e) {
			modelAndView.addObject("MunicipioTarifa", null);
			saveError(e, "No se encontró al municipio con id: " + String.valueOf(id));
		}

		return modelAndView;
	}

	@PostMapping("/configuracion/municipioRegistro/{id}")
	public ModelAndView actualizarMunicipio(@Valid @ModelAttribute("MunicipioTarifa") MunicipioTarifa municipioTarifa,
			BindingResult bindingResult, @PathVariable Integer id, Model model, HttpSession session,
			HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView("configuracion/municipio");

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Editar();

		if (acceso == true) {
			Integer iduser = Integer.parseInt(session.getAttribute("idusuario").toString());

			if (bindingResult.hasErrors()) {

				modelAndView.setViewName("configuracion/municipioRegistro");
				modelAndView.addObject("MunicipioTarifa", municipioTarifa);

				return modelAndView;
			}

			try {

				Municipio municipio = new Municipio();
				municipio.setMuni_Id(municipioTarifa.getMuni_Id());
				municipio.setMuni_Nombre(municipioTarifa.getMuni_Nombre());
				municipio.setMuni_Activo(municipioTarifa.getMuni_Activo());
				municipio.setMuni_UsuarioCreacion(iduser);

				ResponseMunicipio response = municipioService.actualizarMunicipio(municipio);
				modelAndView.addObject("mensaje", null);

				if (response.getCodError() != null) {
					if (response.getCodError() >= 0 && response.getCodError() <= 1) {
						modelAndView.setViewName("configuracion/municipioRegistro");
						modelAndView.addObject("Municipio", municipio);

						if (response.getCodError() == 0) {
							modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
						} else if (response.getCodError() == 1) {
							modelAndView.addObject("mensaje", "El Nombre ya existe");
						}

						return modelAndView;

					}
				} else {

					Tarifa tarifa = new Tarifa();
					tarifa.setTari_Id(municipioTarifa.getTari_Id());
					tarifa.setMuni_Id(municipioTarifa.getMuni_Id());
					tarifa.setTari_Monto(municipioTarifa.getTari_Monto());
					tarifa.setTari_UsuarioEdicion(iduser);

					ResponseTarifa response2 = tarifaService.actualizarTarifa(tarifa);

					if (response2.getCodError() != null) {

						if (response2.getCodError() >= 0 && response2.getCodError() <= 2) {
							modelAndView.setViewName("configuracion/municipioRegistro");
							modelAndView.addObject("MunicipioTarifa", municipioTarifa);

							if (response2.getCodError() == 0) {
								modelAndView.addObject("mensaje", "Se ha producido un error, Intentelo más tarde");
							} else if (response2.getCodError() == 1) {
								modelAndView.addObject("mensaje", "La tarifa ya existe");
							}

							return modelAndView;

						}

					} else {
						modelAndView.addObject("mensajeActualizar", "Actualizado");
					}

				}

			} catch (Exception e) {
				saveError(e, "No se pudo actualizar los datos del municipio");
			}
		} else {
			modelAndView.setViewName("configuracion/municipioRegistro");
			modelAndView.addObject("MunicipioTarifa", municipioTarifa);
			modelAndView.addObject("mensaje", "Usted no tiene los accesos necesarios para este recurso");
		}
		return modelAndView;

	}

	@RequestMapping(value = "/configuracion/eliminarMunicipio", method = RequestMethod.GET)
	@ResponseBody
	public String eliminarMunicipio(@RequestParam(name = "id", required = true) int id, HttpSession session) {

		Integer idPerfilSession = Integer.parseInt(session.getAttribute("idperfil").toString());

		boolean acceso = this.perfilMenuService.seleccionarAccesosPorMenu(idPerfilSession, idmenu).getPerfilMenu()
				.getPmen_Eliminar();

		if (acceso == true) {

			try {

				Tarifa tarifa = this.tarifaService.seleccionarTarifaPorMunicipio(id);

				ResponseMunicipio response = this.municipioService.eliminarMunicipio(id, tarifa.getTari_Id());

				if (response.getCodError() != null) {
					if (response.getCodError() == 0) {
						return "Se ha producido un error, Intentelo más tarde";
					} else if (response.getCodError() == 1) {
						return "El municipio a eliminar no existe";
					} else if (response.getCodError() == 2) {
						return "El municipio tiene dependencias en zonas";
					}
				}

			} catch (Exception e) {
				saveError(e, "No se pudo eliminar al municipio con id: " + String.valueOf(id));
			}

		} else {
			return "No tiene los accesos para este recurso";
		}
		return "";
	}

	public void saveError(Exception e, String informacion) {
		LOGGER.error(
				"\n***************************************************************************************************"
						+ "\nInformación: " + informacion + "\nMensaje: " + e.getMessage() + "\nCausa: " + e.getCause()
						+ "\n\n");
	}
}
