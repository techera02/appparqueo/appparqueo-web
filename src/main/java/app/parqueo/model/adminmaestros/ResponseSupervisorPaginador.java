package app.parqueo.model.adminmaestros;

import java.sql.Timestamp;

import app.parqueo.model.ResponsePaginado;

public class ResponseSupervisorPaginador extends ResponsePaginado{

	private int supe_Id;
	private String supe_Nombres;
	private String supe_ApellidoPaterno;
	private String supe_ApellidoMaterno;
	private int tdoc_Id;
	private String supe_NroDocumento;
	private String supe_TokenFirebase;
	private boolean supe_Activo;
	private int supe_UsuarioCreacion;
	private int supe_UsuarioEdicion;
	private Timestamp supe_FechaCreacion;
	private Timestamp supe_FechaEdicion;
	private String supe_Correo;
	private String supe_Clave;
	private String supe_Alias;
	
	public Integer getSupe_Id() 
	{
		return supe_Id;
	}
	public void setSupe_Id(Integer supe_Id) 
	{
		this.supe_Id = supe_Id;
	}
	
	public String getSupe_Nombres() 
	{
		return supe_Nombres;
	}
	public void setSupe_Nombres(String supe_Nombres) 
	{
		this.supe_Nombres = supe_Nombres;
	}
	
	public String getSupe_ApellidoPaterno() 
	{
		return supe_ApellidoPaterno;
	}
	public void setSupe_ApellidoPaterno(String supe_ApellidoPaterno) 
	{
		this.supe_ApellidoPaterno = supe_ApellidoPaterno;
	}
	
	public String getSupe_ApellidoMaterno() 
	{
		return supe_ApellidoMaterno;
	}
	public void setSupe_ApellidoMaterno(String supe_ApellidoMaterno) 
	{
		this.supe_ApellidoMaterno = supe_ApellidoMaterno;
	}
	
	public Integer getTdoc_Id() 
	{
		return tdoc_Id;
	}
	public void setTdoc_Id(Integer tdoc_Id) 
	{
		this.tdoc_Id = tdoc_Id;
	}
	
	public String getSupe_NroDocumento() 
	{
		return supe_NroDocumento;
	}
	public void setSupe_NroDocumento(String supe_NroDocumento) 
	{
		this.supe_NroDocumento = supe_NroDocumento;
	}
	
	public String getSupe_TokenFirebase() 
	{
		return supe_TokenFirebase;
	}
	public void setSupe_TokenFirebase(String supe_TokenFirebase) 
	{
		this.supe_TokenFirebase = supe_TokenFirebase;
	}
	
	public boolean getSupe_Activo() 
	{
		return supe_Activo;
	}
	public void setSupe_Activo(boolean supe_Activo) 
	{
		this.supe_Activo = supe_Activo;
	}
	
	public Integer getSupe_UsuarioCreacion() 
	{
		return supe_UsuarioCreacion;
	}
	public void setSupe_UsuarioCreacion(Integer supe_UsuarioCreacion) 
	{
		this.supe_UsuarioCreacion = supe_UsuarioCreacion;
	}
	
	public Integer getSupe_UsuarioEdicion() 
	{
		return supe_UsuarioEdicion;
	}
	public void setSupe_UsuarioEdicion(Integer supe_UsuarioEdicion) 
	{
		this.supe_UsuarioEdicion = supe_UsuarioEdicion;
	}
	
	public Timestamp getSupe_FechaCreacion() 
	{
		return supe_FechaCreacion;
	}
	public void setSupe_FechaCreacion(Timestamp supe_FechaCreacion) 
	{
		this.supe_FechaCreacion = supe_FechaCreacion;
	}
	
	public Timestamp getSupe_FechaEdicion() 
	{
		return supe_FechaEdicion;
	}
	public void setSupe_FechaEdicion(Timestamp supe_FechaEdicion) 
	{
		this.supe_FechaEdicion = supe_FechaEdicion;
	}
	
	public String getSupe_Correo() 
	{
		return supe_Correo;
	}
	public void setSupe_Correo(String supe_Correo) 
	{
		this.supe_Correo = supe_Correo;
	}
	
	public String getSupe_Clave() {
		return supe_Clave;
	}

	public void setSupe_Clave(String supe_Clave) {
		this.supe_Clave = supe_Clave;
	}

	public String getSupe_Alias() {
		return supe_Alias;
	}

	public void setSupe_Alias(String supe_Alias) {
		this.supe_Alias = supe_Alias;
	}
}
