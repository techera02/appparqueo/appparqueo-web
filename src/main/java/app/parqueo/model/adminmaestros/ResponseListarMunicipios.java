package app.parqueo.model.adminmaestros;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarMunicipios extends ResponseError{

	private List<ResponseMunicipioPaginador> lista;

	public List<ResponseMunicipioPaginador> getLista() {
		return lista;
	}

	public void setLista(List<ResponseMunicipioPaginador> lista) {
		this.lista = lista;
	}
}
