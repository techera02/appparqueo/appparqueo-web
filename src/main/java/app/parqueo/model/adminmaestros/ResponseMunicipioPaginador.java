package app.parqueo.model.adminmaestros;

import java.math.BigDecimal;
import java.sql.Timestamp;

import app.parqueo.model.ResponsePaginado;

public class ResponseMunicipioPaginador extends ResponsePaginado{

	private int muni_Id;
	private String muni_Nombre;
	private boolean muni_Activo;
	private int muni_UsuarioCreacion;
	private int muni_UsuarioEdicion;
	private Timestamp muni_FechaCreacion;
	private Timestamp muni_FechaEdicion;
	private Integer tari_Id;
	private BigDecimal tari_Monto;
	
	public int getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(int muni_Id) {
		this.muni_Id = muni_Id;
	}
	public String getMuni_Nombre() {
		return muni_Nombre;
	}
	public void setMuni_Nombre(String muni_Nombre) {
		this.muni_Nombre = muni_Nombre;
	}
	public boolean getMuni_Activo() {
		return muni_Activo;
	}
	public void setMuni_Activo(boolean muni_Activo) {
		this.muni_Activo = muni_Activo;
	}
	public int getMuni_UsuarioCreacion() {
		return muni_UsuarioCreacion;
	}
	public void setMuni_UsuarioCreacion(int muni_UsuarioCreacion) {
		this.muni_UsuarioCreacion = muni_UsuarioCreacion;
	}
	public int getMuni_UsuarioEdicion() {
		return muni_UsuarioEdicion;
	}
	public void setMuni_UsuarioEdicion(int muni_UsuarioEdicion) {
		this.muni_UsuarioEdicion = muni_UsuarioEdicion;
	}
	public Timestamp getMuni_FechaCreacion() {
		return muni_FechaCreacion;
	}
	public void setMuni_FechaCreacion(Timestamp muni_FechaCreacion) {
		this.muni_FechaCreacion = muni_FechaCreacion;
	}
	public Timestamp getMuni_FechaEdicion() {
		return muni_FechaEdicion;
	}
	public void setMuni_FechaEdicion(Timestamp muni_FechaEdicion) {
		this.muni_FechaEdicion = muni_FechaEdicion;
	}
	public Integer getTari_Id() {
		return tari_Id;
	}
	public void setTari_Id(Integer tari_Id) {
		this.tari_Id = tari_Id;
	}
	public BigDecimal getTari_Monto() {
		return tari_Monto;
	}
	public void setTari_Monto(BigDecimal tari_Monto) {
		this.tari_Monto = tari_Monto;
	}
}
