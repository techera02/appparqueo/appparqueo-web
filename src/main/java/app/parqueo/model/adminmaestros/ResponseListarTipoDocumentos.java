package app.parqueo.model.adminmaestros;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarTipoDocumentos extends ResponseError{

	private List<TipoDocumento> lista;

	public List<TipoDocumento> getLista() {
		return lista;
	}

	public void setLista(List<TipoDocumento> lista) {
		this.lista = lista;
	}
}
