package app.parqueo.model.adminmaestros;

import app.parqueo.model.ResponseError;

public class ResponseSeleccionarIncidencia extends ResponseError{

	private ResponseIncidencia2 incidencia;

	public ResponseIncidencia2 getIncidencia() {
		return incidencia;
	}

	public void setIncidencia(ResponseIncidencia2 incidencia) {
		this.incidencia = incidencia;
	}
}
