package app.parqueo.model.adminmaestros;

public class ResponseSeleccionarSupervisor {

	private Supervisor supervisor;

	public Supervisor getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}
}
