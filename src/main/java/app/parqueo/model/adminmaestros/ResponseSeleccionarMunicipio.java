package app.parqueo.model.adminmaestros;

public class ResponseSeleccionarMunicipio {

	private Municipio municipio;

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
}
