package app.parqueo.model.adminmaestros;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarSupervisores extends ResponseError{

	private List<ResponseSupervisorPaginador> lista;

	public List<ResponseSupervisorPaginador> getLista() {
		return lista;
	}

	public void setLista(List<ResponseSupervisorPaginador> lista) {
		this.lista = lista;
	}
}
