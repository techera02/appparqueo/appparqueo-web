package app.parqueo.model.adminmaestros;

import app.parqueo.model.ResponseError;

public class ResponseSupervisor extends ResponseError{

	private Integer Supe_Id;	
	
	public Integer getSupe_Id() {
		return Supe_Id;
	}
	public void setSupe_Id(Integer supe_Id) {
		Supe_Id = supe_Id;
	}
}
