package app.parqueo.model.adminmaestros;

public class TipoDocumento {

	private int tdoc_Id;
	private String tdoc_Nombre;
	
	public int getTdoc_Id() {
		return tdoc_Id;
	}
	public void setTdoc_Id(int tdoc_Id) {
		this.tdoc_Id = tdoc_Id;
	}
	public String getTdoc_Nombre() {
		return tdoc_Nombre;
	}
	public void setTdoc_Nombre(String tdoc_Nombre) {
		this.tdoc_Nombre = tdoc_Nombre;
	}
}
