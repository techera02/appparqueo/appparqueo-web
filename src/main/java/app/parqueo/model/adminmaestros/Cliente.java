package app.parqueo.model.adminmaestros;


import java.sql.Timestamp;

public class Cliente {
	
	private Integer clie_Id;
	private String clie_Nombres;
	private String clie_ApePaterno;
	private String clie_ApeMaterno;
	private Integer tdoc_Id;
	private String clie_NroDocumento;
	private String clie_TokenFirebase;
	private Timestamp clie_FechaCreacion;
	private Timestamp clie_FechaEdicion;
	private String clie_NroTelefono;
	private String clie_Correo;
	
	
	public Integer getClie_Id() {
		return clie_Id;
	}
	public void setClie_Id(Integer clie_Id) {
		this.clie_Id = clie_Id;
	}
	public String getClie_Nombres() {
		return clie_Nombres;
	}
	public void setClie_Nombres(String clie_Nombres) {
		this.clie_Nombres = clie_Nombres;
	}
	public String getClie_ApePaterno() {
		return clie_ApePaterno;
	}
	public void setClie_ApePaterno(String clie_ApePaterno) {
		this.clie_ApePaterno = clie_ApePaterno;
	}
	public String getClie_ApeMaterno() {
		return clie_ApeMaterno;
	}
	public void setClie_ApeMaterno(String clie_ApeMaterno) {
		this.clie_ApeMaterno = clie_ApeMaterno;
	}
	public Integer getTdoc_Id() {
		return tdoc_Id;
	}
	public void setTdoc_Id(Integer tdoc_Id) {
		this.tdoc_Id = tdoc_Id;
	}
	public String getClie_NroDocumento() {
		return clie_NroDocumento;
	}
	public void setClie_NroDocumento(String clie_NroDocumento) {
		this.clie_NroDocumento = clie_NroDocumento;
	}
	public String getClie_TokenFirebase() {
		return clie_TokenFirebase;
	}
	public void setClie_TokenFirebase(String clie_TokenFirebase) {
		this.clie_TokenFirebase = clie_TokenFirebase;
	}
	public Timestamp getClie_FechaCreacion() {
		return clie_FechaCreacion;
	}
	public void setClie_FechaCreacion(Timestamp clie_FechaCreacion) {
		this.clie_FechaCreacion = clie_FechaCreacion;
	}
	public Timestamp getClie_FechaEdicion() {
		return clie_FechaEdicion;
	}
	public void setClie_FechaEdicion(Timestamp clie_FechaEdicion) {
		this.clie_FechaEdicion = clie_FechaEdicion;
	}
	public String getClie_NroTelefono() {
		return clie_NroTelefono;
	}
	public void setClie_NroTelefono(String clie_NroTelefono) {
		this.clie_NroTelefono = clie_NroTelefono;
	}
	public String getClie_Correo() {
		return clie_Correo;
	}
	public void setClie_Correo(String clie_Correo) {
		this.clie_Correo = clie_Correo;
	}
}

