package app.parqueo.model.adminmaestros;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarCliente extends ResponseError{
	
	private List<ResponseCliente> lista;

	public List<ResponseCliente> getLista() {
		return lista;
	}

	public void setLista(List<ResponseCliente> lista) {
		this.lista = lista;
	}
}
