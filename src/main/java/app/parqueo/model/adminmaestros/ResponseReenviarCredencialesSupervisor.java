package app.parqueo.model.adminmaestros;

import app.parqueo.model.ResponseError;

public class ResponseReenviarCredencialesSupervisor extends ResponseError{

	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
