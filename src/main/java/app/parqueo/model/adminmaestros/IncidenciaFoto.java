package app.parqueo.model.adminmaestros;

public class IncidenciaFoto {
	private Integer ifot_Id;
	private Integer inci_Id;
	private String ifot_NombreArchivo;
	
	public Integer getIfot_Id() {
		return ifot_Id;
	}
	public void setIfot_Id(Integer ifot_Id) {
		this.ifot_Id = ifot_Id;
	}
	public Integer getInci_Id() {
		return inci_Id;
	}
	public void setInci_Id(Integer inci_id) {
		this.inci_Id = inci_id;
	}	
	public String getIfot_NombreArchivo() {
		return ifot_NombreArchivo;
	}
	public void setIfot_NombreArchivo(String ifot_nombrearchivo) {
		this.ifot_NombreArchivo = ifot_nombrearchivo;
	}
}
