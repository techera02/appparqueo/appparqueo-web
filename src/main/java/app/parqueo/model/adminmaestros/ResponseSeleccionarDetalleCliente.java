package app.parqueo.model.adminmaestros;

import app.parqueo.model.ResponseError;

public class ResponseSeleccionarDetalleCliente extends ResponseError{

	private ResponseDetalleCliente detalleCliente;

	public ResponseDetalleCliente getDetalleCliente() {
		return detalleCliente;
	}

	public void setDetalleCliente(ResponseDetalleCliente detalleCliente) {
		this.detalleCliente = detalleCliente;
	}

}
