package app.parqueo.model.adminmaestros;

import java.sql.Timestamp;

import app.parqueo.model.ResponsePaginado;

public class ResponseIncidencia extends ResponsePaginado {
	private Integer inci_Id;
	private Integer tinc_Id;
	private Integer supe_Id;
	private Integer clie_Id;
	private Timestamp inci_FechaCreacion;
	private String inci_Descripcion;
	private String inci_Placa;
	private String tinc_Nombre;
	
	public Integer getInci_Id() {
		return inci_Id;
	}	
	public void setInci_Id(Integer inci_Id) {
		this.inci_Id = inci_Id;
	}
	public Integer getTinc_Id() {
		return tinc_Id;
	}
	public void setTinc_Id(Integer tinc_id) {
		this.tinc_Id = tinc_id;
	}
	public Integer getSupe_Id() {
		return supe_Id;
	}
	public void setSupe_Id(Integer supe_id) {
		this.supe_Id = supe_id;
	}
	public Integer getClie_Id() {
		return clie_Id;
	}
	public void setClie_Id(Integer clie_id) {
		this.clie_Id = clie_id;
	}
	public Timestamp getInci_FechaCreacion() {
		return inci_FechaCreacion;
	}	
	public void setInci_FechaCreacion(Timestamp inci_FechaCreacion) {
		this.inci_FechaCreacion = inci_FechaCreacion;
	}	
	public String getInci_Descripcion() {
		return inci_Descripcion;
	}	
	public void setInci_Descripcion(String inci_descripcion) {
		this.inci_Descripcion = inci_descripcion;
	}
	public String getInci_Placa() {
		return inci_Placa;
	}	
	public void setInci_Placa(String inci_placa) {
		this.inci_Placa = inci_placa;
	}
	public String getTinc_Nombre() {
		return tinc_Nombre;
	}
	public void setTinc_Nombre(String tinc_Nombre) {
		this.tinc_Nombre = tinc_Nombre;
	}

}
