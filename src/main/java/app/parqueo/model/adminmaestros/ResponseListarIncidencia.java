package app.parqueo.model.adminmaestros;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarIncidencia extends ResponseError{
	private List<ResponseIncidencia> lista;

	public List<ResponseIncidencia> getLista() {
		return lista;
	}

	public void setLista(List<ResponseIncidencia> lista) {
		this.lista = lista;
	}
}
