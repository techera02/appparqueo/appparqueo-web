package app.parqueo.model.adminmaestros;

import app.parqueo.model.ResponseError;

public class ResponseMunicipio extends ResponseError{

	private Integer Muni_Id;

	public Integer getMuni_Id() {
		return Muni_Id;
	}

	public void setMuni_Id(Integer muni_Id) {
		Muni_Id = muni_Id;
	}
}
