package app.parqueo.model;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class MunicipioTarifa {

	//Municipio
	private int muni_Id;	
	@Pattern(regexp = "[a-zA-Z_0-9\s]+", message="El nombre del municipio solo puede tener letras mayúsculas,minúsculas o números.")
	@NotEmpty(message="El nombre del municipio es obligatorio.")
	@Size(min = 1, max = 50, message = "El nombre debe medir entre 1 y 50")
	private String muni_Nombre;
	private boolean muni_Activo;
	private int muni_UsuarioCreacion;
	private int muni_UsuarioEdicion;
	
	//Tarifa
	private Integer tari_Id;
	//private Integer muni_Id;
	@Digits(integer=10, fraction=2, message="Los decimales deben ser solo de 2 dígitos (Ejemplo: .00)")
	@NotNull(message="La tarifa del municipio es obligatorio.")
	private BigDecimal tari_Monto;
	private Integer tari_UsuarioCreacion;
	private Integer tari_UsuarioEdicion;
	
	public int getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(int muni_Id) {
		this.muni_Id = muni_Id;
	}
	public String getMuni_Nombre() {
		return muni_Nombre;
	}
	public void setMuni_Nombre(String muni_Nombre) {
		this.muni_Nombre = muni_Nombre;
	}
	public boolean getMuni_Activo() {
		return muni_Activo;
	}
	public void setMuni_Activo(boolean muni_Activo) {
		this.muni_Activo = muni_Activo;
	}
	public int getMuni_UsuarioCreacion() {
		return muni_UsuarioCreacion;
	}
	public void setMuni_UsuarioCreacion(int muni_UsuarioCreacion) {
		this.muni_UsuarioCreacion = muni_UsuarioCreacion;
	}
	public int getMuni_UsuarioEdicion() {
		return muni_UsuarioEdicion;
	}
	public void setMuni_UsuarioEdicion(int muni_UsuarioEdicion) {
		this.muni_UsuarioEdicion = muni_UsuarioEdicion;
	}
	
	public Integer getTari_Id() {
		return tari_Id;
	}
	public void setTari_Id(Integer tari_Id) {
		this.tari_Id = tari_Id;
	}

	public BigDecimal getTari_Monto() {
		return tari_Monto;
	}
	public void setTari_Monto(BigDecimal tari_Monto) {
		this.tari_Monto = tari_Monto;
	}

	public Integer getTari_UsuarioCreacion() {
		return tari_UsuarioCreacion;
	}
	public void setTari_UsuarioCreacion(Integer tari_UsuarioCreacion) {
		this.tari_UsuarioCreacion = tari_UsuarioCreacion;
	}

	public Integer getTari_UsuarioEdicion() {
		return tari_UsuarioEdicion;
	}
	public void setTari_UsuarioEdicion(Integer tari_UsuarioEdicion) {
		this.tari_UsuarioEdicion = tari_UsuarioEdicion;
	}	
}
