package app.parqueo.model.cliente;

import java.util.List;

import app.parqueo.model.ResponseError;


public class ResponseListarVehiculosPorCliente extends ResponseError{
    private List<Vehiculo> vehiculos;

	public List<Vehiculo> getVehiculos() {
		return vehiculos;
	}
	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}	
}

