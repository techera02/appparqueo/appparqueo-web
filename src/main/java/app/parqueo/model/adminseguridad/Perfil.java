package app.parqueo.model.adminseguridad;

import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class Perfil {
	private Integer perf_Id;
	
	@NotEmpty(message = "El Nombre no debe estar vacío")
    @Size(min = 2, max = 30, message = "El Nombre debe medir entre 2 y 30")
	private String perf_Nombre;
	
	private boolean perf_Activo;
	private Integer perf_UsuarioCreacion;
	private Integer perf_UsuarioEdicion;
	private Timestamp perf_FechaCreacion;
	private Timestamp perf_FechaEdicion;
	
	public Integer getPerf_Id() {
		return perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	public String getPerf_Nombre() {
		return perf_Nombre;
	}
	public void setPerf_Nombre(String perf_Nombre) {
		this.perf_Nombre = perf_Nombre;
	}
	public boolean isPerf_Activo() {
		return perf_Activo;
	}
	public void setPerf_Activo(boolean perf_Activo) {
		this.perf_Activo = perf_Activo;
	}
	public Integer getPerf_UsuarioCreacion() {
		return perf_UsuarioCreacion;
	}
	public void setPerf_UsuarioCreacion(Integer perf_UsuarioCreacion) {
		this.perf_UsuarioCreacion = perf_UsuarioCreacion;
	}
	public Integer getPerf_UsuarioEdicion() {
		return perf_UsuarioEdicion;
	}
	public void setPerf_UsuarioEdicion(Integer perf_UsuarioEdicion) {
		this.perf_UsuarioEdicion = perf_UsuarioEdicion;
	}
	public Timestamp getPerf_FechaCreacion() {
		return perf_FechaCreacion;
	}
	public void setPerf_FechaCreacion(Timestamp perf_FechaCreacion) {
		this.perf_FechaCreacion = perf_FechaCreacion;
	}
	public Timestamp getPerf_FechaEdicion() {
		return perf_FechaEdicion;
	}
	public void setPerf_FechaEdicion(Timestamp perf_FechaEdicion) {
		this.perf_FechaEdicion = perf_FechaEdicion;
	}

}
