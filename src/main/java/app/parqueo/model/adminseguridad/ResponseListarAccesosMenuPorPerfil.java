package app.parqueo.model.adminseguridad;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarAccesosMenuPorPerfil extends ResponseError{
	private List<ResponsePerfilMenu> lista;

	public List<ResponsePerfilMenu> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfilMenu> lista) {
		this.lista = lista;
	}
	
}
