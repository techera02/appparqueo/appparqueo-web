package app.parqueo.model.adminseguridad;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarPerfiles extends ResponseError{
	private List<ResponsePerfilPaginador> lista;

	public List<ResponsePerfilPaginador> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfilPaginador> lista) {
		this.lista = lista;
	}
	
	
}
