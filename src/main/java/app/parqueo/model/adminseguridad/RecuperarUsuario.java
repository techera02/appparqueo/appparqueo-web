package app.parqueo.model.adminseguridad;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class RecuperarUsuario {
	
	@NotEmpty(message = "El Correo no debe estar vacío")
	@Email
	public String usua_correo;

	public String getUsua_correo() {
		return usua_correo;
	}

	public void setUsua_correo(String usua_correo) {
		this.usua_correo = usua_correo;
	}

}
