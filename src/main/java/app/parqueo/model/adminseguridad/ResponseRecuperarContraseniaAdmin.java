package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponseRecuperarContraseniaAdmin extends ResponseError{
	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
