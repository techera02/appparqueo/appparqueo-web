package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponseInsertarPerfil extends ResponseError{
	private Integer perf_Id;

	public Integer getPerf_Id() {
		return perf_Id;
	}

	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	
}
