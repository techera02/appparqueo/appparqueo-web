package app.parqueo.model.adminseguridad;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarConfiguracionMenuPorPerfil extends ResponseError{
	private List<ResponsePerfilMenu2> lista;

	public List<ResponsePerfilMenu2> getLista() {
		return lista;
	}

	public void setLista(List<ResponsePerfilMenu2> lista) {
		this.lista = lista;
	}

}
