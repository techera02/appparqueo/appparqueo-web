package app.parqueo.model.adminseguridad;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarUsuarios extends ResponseError{
	private List<ResponseUsuarioPaginador> lista;

	public List<ResponseUsuarioPaginador> getLista() {
		return lista;
	}

	public void setLista(List<ResponseUsuarioPaginador> lista) {
		this.lista = lista;
	}
}
