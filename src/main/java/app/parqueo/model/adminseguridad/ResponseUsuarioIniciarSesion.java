package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponseUsuarioIniciarSesion extends ResponseError{
	private UsuarioLogin usuario;

	public UsuarioLogin getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioLogin usuario) {
		this.usuario = usuario;
	}
	
}
