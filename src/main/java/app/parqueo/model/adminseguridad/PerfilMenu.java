package app.parqueo.model.adminseguridad;

public class PerfilMenu {
	private Integer pmen_Id;
	private Integer perf_Id;
	private Integer menu_Id;
	private Integer menu_PadreId;
	private boolean pmen_Crear;
	private boolean pmen_Editar;
	private boolean pmen_Eliminar;
	private boolean pmen_Activo;
	private Integer pmen_UsuarioCreacion;
	private Integer pmen_UsuarioEdicion;

	
	public Integer getPmen_Id() {
		return pmen_Id;
	}
	public void setPmen_Id(Integer pmen_Id) {
		this.pmen_Id = pmen_Id;
	}
	public Integer getPerf_Id() {
		return perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	public Integer getMenu_Id() {
		return menu_Id;
	}
	public void setMenu_Id(Integer menu_Id) {
		this.menu_Id = menu_Id;
	}
	public boolean getPmen_Crear() {
		return pmen_Crear;
	}
	public void setPmen_Crear(boolean pmen_Crear) {
		this.pmen_Crear = pmen_Crear;
	}
	public boolean getPmen_Editar() {
		return pmen_Editar;
	}
	public void setPmen_Editar(boolean pmen_Editar) {
		this.pmen_Editar = pmen_Editar;
	}
	public boolean getPmen_Eliminar() {
		return pmen_Eliminar;
	}
	public void setPmen_Eliminar(boolean pmen_Eliminar) {
		this.pmen_Eliminar = pmen_Eliminar;
	}
	public boolean getPmen_Activo() {
		return pmen_Activo;
	}
	public void setPmen_Activo(boolean pmen_Activo) {
		this.pmen_Activo = pmen_Activo;
	}
	public Integer getPmen_UsuarioCreacion() {
		return pmen_UsuarioCreacion;
	}
	public void setPmen_UsuarioCreacion(Integer pmen_UsuarioCreacion) {
		this.pmen_UsuarioCreacion = pmen_UsuarioCreacion;
	}
	public Integer getPmen_UsuarioEdicion() {
		return pmen_UsuarioEdicion;
	}
	public void setPmen_UsuarioEdicion(Integer pmen_UsuarioEdicion) {
		this.pmen_UsuarioEdicion = pmen_UsuarioEdicion;
	}
	public Integer getMenu_PadreId() {
		return menu_PadreId;
	}
	public void setMenu_PadreId(Integer menu_PadreId) {
		this.menu_PadreId = menu_PadreId;
	}
	
}
