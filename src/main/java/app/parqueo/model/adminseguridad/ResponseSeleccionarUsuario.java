package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponseSeleccionarUsuario extends ResponseError{
	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
