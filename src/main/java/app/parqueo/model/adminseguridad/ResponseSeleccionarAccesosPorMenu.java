package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponseSeleccionarAccesosPorMenu extends ResponseError {
	private PerfilMenu perfilMenu;

	public PerfilMenu getPerfilMenu() {
		return perfilMenu;
	}

	public void setPerfilMenu(PerfilMenu perfilMenu) {
		this.perfilMenu = perfilMenu;
	}
	
}
