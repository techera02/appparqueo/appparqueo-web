package app.parqueo.model.adminseguridad;

import java.sql.Timestamp;

import app.parqueo.model.ResponseError;

public class ResponsePerfilPaginador extends ResponseError {
	private Integer rowNumber;
	private Integer rowCount;
	private Integer pageCount;
	private Integer pageIndex;
	private Integer perf_Id;
	private String perf_Nombre;
	private boolean perf_Activo;
	private Integer perf_UsuarioCreacion;
	private Integer perf_UsuarioEdicion;
	private Timestamp perf_FechaCreacion;
	private Timestamp perf_FechaEdicion;
	public Integer getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}
	public Integer getRowCount() {
		return rowCount;
	}
	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}
	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}
	public Integer getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}
	public Integer getPerf_Id() {
		return perf_Id;
	}
	public void setPerf_Id(Integer perf_Id) {
		this.perf_Id = perf_Id;
	}
	public String getPerf_Nombre() {
		return perf_Nombre;
	}
	public void setPerf_Nombre(String perf_Nombre) {
		this.perf_Nombre = perf_Nombre;
	}
	public boolean getPerf_Activo() {
		return perf_Activo;
	}
	public void setPerf_Activo(boolean perf_Activo) {
		this.perf_Activo = perf_Activo;
	}
	public Integer getPerf_UsuarioCreacion() {
		return perf_UsuarioCreacion;
	}
	public void setPerf_UsuarioCreacion(Integer perf_UsuarioCreacion) {
		this.perf_UsuarioCreacion = perf_UsuarioCreacion;
	}
	public Integer getPerf_UsuarioEdicion() {
		return perf_UsuarioEdicion;
	}
	public void setPerf_UsuarioEdicion(Integer perf_UsuarioEdicion) {
		this.perf_UsuarioEdicion = perf_UsuarioEdicion;
	}
	public Timestamp getPerf_FechaCreacion() {
		return perf_FechaCreacion;
	}
	public void setPerf_FechaCreacion(Timestamp perf_FechaCreacion) {
		this.perf_FechaCreacion = perf_FechaCreacion;
	}
	public Timestamp getPerf_FechaEdicion() {
		return perf_FechaEdicion;
	}
	public void setPerf_FechaEdicion(Timestamp perf_FechaEdicion) {
		this.perf_FechaEdicion = perf_FechaEdicion;
	}
	
	
}
