package app.parqueo.model.adminseguridad;

import app.parqueo.model.ResponseError;

public class ResponsePerfil extends ResponseError {
	
	private Perfil perfil;

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	
}
