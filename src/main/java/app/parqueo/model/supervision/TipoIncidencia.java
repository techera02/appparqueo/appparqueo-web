package app.parqueo.model.supervision;

import java.sql.Timestamp;

public class TipoIncidencia {
	private Integer tinc_Id;
	private String tinc_Nombre;
	private boolean tinc_Eliminado;
	private Integer tinc_UsuarioCreacion;
	private Integer tinc_UsuarioEdicion;
	private Integer tinc_UsuarioElimina;
	private Timestamp tinc_FechaCreacion;
	private Timestamp tinc_FechaEdicion;
	private Timestamp tinc_FechaElimina;
	
	public Integer getTinc_Id() {
		return tinc_Id;
	}
	public void setTinc_Id(Integer tinc_Id) {
		this.tinc_Id = tinc_Id;
	}
	public String getTinc_Nombre() {
		return tinc_Nombre;
	}
	public void setTinc_Nombre(String tinc_Nombre) {
		this.tinc_Nombre = tinc_Nombre;
	}
	public boolean isTinc_Eliminado() {
		return tinc_Eliminado;
	}
	public void setTinc_Eliminado(boolean tinc_Eliminado) {
		this.tinc_Eliminado = tinc_Eliminado;
	}
	public Integer getTinc_UsuarioCreacion() {
		return tinc_UsuarioCreacion;
	}
	public void setTinc_UsuarioCreacion(Integer tinc_UsuarioCreacion) {
		this.tinc_UsuarioCreacion = tinc_UsuarioCreacion;
	}
	public Integer getTinc_UsuarioEdicion() {
		return tinc_UsuarioEdicion;
	}
	public void setTinc_UsuarioEdicion(Integer tinc_UsuarioEdicion) {
		this.tinc_UsuarioEdicion = tinc_UsuarioEdicion;
	}
	public Integer getTinc_UsuarioElimina() {
		return tinc_UsuarioElimina;
	}
	public void setTinc_UsuarioElimina(Integer tinc_UsuarioElimina) {
		this.tinc_UsuarioElimina = tinc_UsuarioElimina;
	}
	public Timestamp getTinc_FechaCreacion() {
		return tinc_FechaCreacion;
	}
	public void setTinc_FechaCreacion(Timestamp tinc_FechaCreacion) {
		this.tinc_FechaCreacion = tinc_FechaCreacion;
	}
	public Timestamp getTinc_FechaEdicion() {
		return tinc_FechaEdicion;
	}
	public void setTinc_FechaEdicion(Timestamp tinc_FechaEdicion) {
		this.tinc_FechaEdicion = tinc_FechaEdicion;
	}
	public Timestamp getTinc_FechaElimina() {
		return tinc_FechaElimina;
	}
	public void setTinc_FechaElimina(Timestamp tinc_FechaElimina) {
		this.tinc_FechaElimina = tinc_FechaElimina;
	}

	
}
