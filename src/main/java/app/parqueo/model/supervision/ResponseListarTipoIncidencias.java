package app.parqueo.model.supervision;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarTipoIncidencias extends ResponseError {
	private List<TipoIncidencia> lista;

	public List<TipoIncidencia> getLista() {
		return lista;
	}

	public void setLista(List<TipoIncidencia> lista) {
		this.lista = lista;
	}
	
}
