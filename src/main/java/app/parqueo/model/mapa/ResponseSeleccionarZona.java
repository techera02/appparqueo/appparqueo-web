package app.parqueo.model.mapa;

import app.parqueo.model.ResponseError;
import app.parqueo.model.adminconfig.Zona;

public class ResponseSeleccionarZona extends ResponseError{
	private Zona zona;

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

}
