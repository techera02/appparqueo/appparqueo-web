package app.parqueo.model.mapa;

import app.parqueo.model.ResponseError;

public class ResponseSeleccionarEstacionamiento extends ResponseError{
	private Estacionamiento estacionamiento;

	public Estacionamiento getEstacionamiento() {
		return estacionamiento;
	}

	public void setEstacionamiento(Estacionamiento estacionamiento) {
		this.estacionamiento = estacionamiento;
	}

}
