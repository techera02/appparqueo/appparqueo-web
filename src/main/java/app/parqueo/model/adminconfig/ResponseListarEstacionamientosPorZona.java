package app.parqueo.model.adminconfig;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarEstacionamientosPorZona extends ResponseError{
	private List<ResponseEstacionamiento> lista;

	public List<ResponseEstacionamiento> getLista() {
		return lista;
	}

	public void setLista(List<ResponseEstacionamiento> lista) {
		this.lista = lista;
	}

}
