package app.parqueo.model.adminconfig;

import app.parqueo.model.ResponseError;

public class ResponseInsertarZona extends ResponseError {
	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}

}
