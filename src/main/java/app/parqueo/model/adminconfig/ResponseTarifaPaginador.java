package app.parqueo.model.adminconfig;

import java.math.BigDecimal;

import app.parqueo.model.ResponsePaginado;

public class ResponseTarifaPaginador extends ResponsePaginado{

	private Integer tari_Id;
	private Integer muni_Id;
	private BigDecimal tari_Monto;
	private Integer tari_UsuarioCreacion;
	private Integer tari_UsuarioEdicion;
	
	public Integer getTari_Id() {
		return tari_Id;
	}
	public void setTari_Id(Integer tari_Id) {
		this.tari_Id = tari_Id;
	}
	public Integer getMuni_Id() {
		return muni_Id;
	}
	public void setMuni_Id(Integer muni_Id) {
		this.muni_Id = muni_Id;
	}
	public BigDecimal getTari_Monto() {
		return tari_Monto;
	}
	public void setTari_Monto(BigDecimal tari_Monto) {
		this.tari_Monto = tari_Monto;
	}
	public Integer getTari_UsuarioCreacion() {
		return tari_UsuarioCreacion;
	}
	public void setTari_UsuarioCreacion(Integer tari_UsuarioCreacion) {
		this.tari_UsuarioCreacion = tari_UsuarioCreacion;
	}
	public Integer getTari_UsuarioEdicion() {
		return tari_UsuarioEdicion;
	}
	public void setTari_UsuarioEdicion(Integer tari_UsuarioEdicion) {
		this.tari_UsuarioEdicion = tari_UsuarioEdicion;
	}
}
