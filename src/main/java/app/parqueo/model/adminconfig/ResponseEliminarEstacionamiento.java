package app.parqueo.model.adminconfig;

import app.parqueo.model.ResponseError;

public class ResponseEliminarEstacionamiento extends ResponseError{
	private boolean resultado;

	public boolean isResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}

}
