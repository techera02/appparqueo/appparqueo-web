package app.parqueo.model.adminconfig;

import app.parqueo.model.ResponseError;

public class ResponseTarifa extends ResponseError{

	private boolean resultado;

	public boolean getResultado() {
		return resultado;
	}

	public void setResultado(boolean resultado) {
		this.resultado = resultado;
	}
}
