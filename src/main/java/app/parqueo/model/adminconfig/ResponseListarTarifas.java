package app.parqueo.model.adminconfig;

import java.util.List;

import app.parqueo.model.ResponseError;

public class ResponseListarTarifas extends ResponseError{

	private List<ResponseTarifaPaginador> lista;

	public List<ResponseTarifaPaginador> getLista() {
		return lista;
	}

	public void setLista(List<ResponseTarifaPaginador> lista) {
		this.lista = lista;
	}
}
