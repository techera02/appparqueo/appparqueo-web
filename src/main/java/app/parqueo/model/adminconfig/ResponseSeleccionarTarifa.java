package app.parqueo.model.adminconfig;

public class ResponseSeleccionarTarifa {

	private Tarifa tarifa;

	public Tarifa getTarifa() {
		return tarifa;
	}

	public void setTarifa(Tarifa tarifa) {
		this.tarifa = tarifa;
	}
}
