package app.parqueo.model.adminconfig;

import java.util.List;

public class ResponseListarZonas {
	private List<ResponseZona> lista;

	public List<ResponseZona> getLista() {
		return lista;
	}

	public void setLista(List<ResponseZona> lista) {
		this.lista = lista;
	}

}
