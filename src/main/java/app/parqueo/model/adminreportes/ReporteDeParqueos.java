package app.parqueo.model.adminreportes;

import java.math.BigDecimal;

import app.parqueo.model.ResponsePaginado;

public class ReporteDeParqueos extends ResponsePaginado {
	public String zona;
	public Integer cantidad_parqueos;
	public String tiempo;
	public BigDecimal monto;
	public BigDecimal monto_cobrado;
	public BigDecimal por_cobrar;
	public BigDecimal deuda_total;
	
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public Integer getCantidad_parqueos() {
		return cantidad_parqueos;
	}
	public void setCantidad_parqueos(Integer cantidad_parqueos) {
		this.cantidad_parqueos = cantidad_parqueos;
	}
	public String getTiempo() {
		return tiempo;
	}
	public void setTiempo(String tiempo) {
		this.tiempo = tiempo;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public BigDecimal getMonto_cobrado() {
		return monto_cobrado;
	}
	public void setMonto_cobrado(BigDecimal monto_cobrado) {
		this.monto_cobrado = monto_cobrado;
	}
	public BigDecimal getPor_cobrar() {
		return por_cobrar;
	}
	public void setPor_cobrar(BigDecimal por_cobrar) {
		this.por_cobrar = por_cobrar;
	}
	public BigDecimal getDeuda_total() {
		return deuda_total;
	}
	public void setDeuda_total(BigDecimal deuda_total) {
		this.deuda_total = deuda_total;
	}
	
	
}
