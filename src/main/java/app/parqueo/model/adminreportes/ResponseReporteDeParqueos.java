package app.parqueo.model.adminreportes;

import java.util.List;

public class ResponseReporteDeParqueos {
	public List<ReporteDeParqueos> lista;

	public List<ReporteDeParqueos> getLista() {
		return lista;
	}

	public void setLista(List<ReporteDeParqueos> lista) {
		this.lista = lista;
	}
}
