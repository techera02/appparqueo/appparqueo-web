package app.parqueo.consumer;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminmaestros.Municipio;
import app.parqueo.model.adminmaestros.ResponseListarMunicipios;
import app.parqueo.model.adminmaestros.ResponseMunicipio;
import app.parqueo.model.adminmaestros.ResponseSeleccionarMunicipio;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class MunicipioConsumer {

	@Value("${adminmaestros-api.url.base}")
	private String urlBaseAdminMaestrosApi;
	
	public ResponseListarMunicipios listarMunicipios(Integer pageNumber,Integer pageSize, String nombre, Integer activo){
		String url = urlBaseAdminMaestrosApi+"Municipio/ListarMunicipio/"
	+pageNumber+"/"+pageSize+"/"+nombre+"/"+activo;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarMunicipios> type = new ParameterizedTypeReference<ResponseListarMunicipios>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarMunicipios> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarMunicipios response = lista.getBody();
	
		return response;
	}
	public ResponseListarMunicipios listarMunicipiosCB(){
		
		Integer rowCount = this.listarMunicipios(1,1,"*",0).getLista().get(0).getRowCount();
		
		String url = urlBaseAdminMaestrosApi+"Municipio/ListarMunicipio/1/"+rowCount+"/*/0";
					
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarMunicipios> type = new ParameterizedTypeReference<ResponseListarMunicipios>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarMunicipios> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarMunicipios response = lista.getBody();
					
		return response;
	}
	public ResponseMunicipio insertarMunicipio(Municipio municipio) {
		String url = urlBaseAdminMaestrosApi+"Municipio/InsertarMunicipio";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseMunicipio> type = new ParameterizedTypeReference<ResponseMunicipio>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Municipio> entity = new HttpEntity<>(municipio,headers);
		
		ResponseEntity<ResponseMunicipio> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseMunicipio response = dep.getBody();
		
		return response;
	}
	
	public ResponseMunicipio actualizarMunicipio(Municipio municipio) {
		String url = urlBaseAdminMaestrosApi+"Municipio/ActualizarMunicipio";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseMunicipio> type = new ParameterizedTypeReference<ResponseMunicipio>() {};	
		
		
		HttpEntity<Municipio> entity = new HttpEntity<Municipio>(municipio, headers);
		
		ResponseEntity<ResponseMunicipio> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseMunicipio response = dep.getBody();

		return response;
	}
	
	public Municipio seleccionarMunicipio(Integer id) {
		String url = urlBaseAdminMaestrosApi+"Municipio/SeleccionarMunicipio/"+id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarMunicipio> type = new ParameterizedTypeReference<ResponseSeleccionarMunicipio>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarMunicipio> municipio = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarMunicipio response = municipio.getBody();
		
		return response.getMunicipio();
	}
	
	public ResponseMunicipio eliminarMunicipio(Integer idmunicipio, Integer idtarifa) {
		String url = urlBaseAdminMaestrosApi+"Municipio/EliminarMunicipio/"+idmunicipio+'/'+idtarifa;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseMunicipio> type = new ParameterizedTypeReference<ResponseMunicipio>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseMunicipio> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseMunicipio response = dep.getBody();
		
		return response;
	}
}
