package app.parqueo.consumer;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminreportes.ResponseReporteDeParqueos;


@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class AdminReportesConsumer {
	
	@Value("${adminreportes-api.url.base}")
	private String urlBaseAdminReportesApi;


	public ResponseReporteDeParqueos reporteDeParqueosZona(Integer pageNumber, Integer pageSize,  Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin){
		
		String url = urlBaseAdminReportesApi+"AdminReportes/ReporteDeParqueos/"+pageNumber+"/"+pageSize+"/"+idmunicipio+
				"/"+fechainicio+"/"+indiceFechainicio+"/"+fechafin+"/"+indiceFechaFin;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseReporteDeParqueos> type = new ParameterizedTypeReference<ResponseReporteDeParqueos>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseReporteDeParqueos> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseReporteDeParqueos response = lista.getBody();
					
		return response;
	}
	
}
