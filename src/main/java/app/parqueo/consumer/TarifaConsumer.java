package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminconfig.ResponseListarTarifas;
import app.parqueo.model.adminconfig.ResponseSeleccionarTarifa;
import app.parqueo.model.adminconfig.ResponseTarifa;
import app.parqueo.model.adminconfig.Tarifa;
@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class TarifaConsumer {

	@Value("${adminconfig-api.url.base}")
	private String urlBaseAdminConfigApi;
	
	public ResponseListarTarifas listarTarifas(Integer pageNumber,Integer pageSize){
		String url = urlBaseAdminConfigApi+"Tarifa/ListarTarifas/"
	+pageNumber+"/"+pageSize;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarTarifas> type = new ParameterizedTypeReference<ResponseListarTarifas>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarTarifas> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarTarifas response = lista.getBody();
	
		return response;
	}
	
	public ResponseTarifa insertarTarifa(Tarifa tarifa) {
		String url = urlBaseAdminConfigApi+"Tarifa/InsertarTarifa";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseTarifa> type = new ParameterizedTypeReference<ResponseTarifa>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Tarifa> entity = new HttpEntity<>(tarifa,headers);
		
		ResponseEntity<ResponseTarifa> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseTarifa response = dep.getBody();
		
		return response;
	}
	
	public ResponseTarifa actualizarTarifa(Tarifa tarifa) {
		String url = urlBaseAdminConfigApi+"Tarifa/ActualizarTarifa";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseTarifa> type = new ParameterizedTypeReference<ResponseTarifa>() {};	
		
		
		HttpEntity<Tarifa> entity = new HttpEntity<Tarifa>(tarifa, headers);
		
		ResponseEntity<ResponseTarifa> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseTarifa response = dep.getBody();

		return response;
	}
	
	public Tarifa seleccionarTarifa(Integer id) {
		String url = urlBaseAdminConfigApi+"Tarifa/SeleccionarTarifa/"+id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarTarifa> type = new ParameterizedTypeReference<ResponseSeleccionarTarifa>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarTarifa> tarifa = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarTarifa response = tarifa.getBody();
		
		return response.getTarifa();
	}
	
	public ResponseTarifa eliminarTarifa(Integer id) {
		String url = urlBaseAdminConfigApi+"Tarifa/EliminarTarifa/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseTarifa> type = new ParameterizedTypeReference<ResponseTarifa>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseTarifa> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseTarifa response = dep.getBody();
		
		return response;
	}
}
