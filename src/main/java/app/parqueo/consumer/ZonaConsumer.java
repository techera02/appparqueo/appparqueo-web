package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminconfig.ResponseEliminarZona;
import app.parqueo.model.adminconfig.ResponseInsertarZona;
import app.parqueo.model.adminconfig.ResponseListarZonas;
import app.parqueo.model.adminconfig.Zona;
import app.parqueo.model.mapa.ResponseSeleccionarZona;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class ZonaConsumer {
	
	@Value("${adminconfig-api.url.base}")
	private String urlBaseAdminConfigApi;
	
	@Value("${mapa-api.url.base}")
	private String urlBaseMapaApi;
	
	public ResponseListarZonas listarZonas(Integer Muni_Id, Integer pageNumber,Integer pageSize, Integer activo){
		String url = urlBaseAdminConfigApi+"Zona/ListarZonas/"+Muni_Id+"/"+pageNumber+"/"+pageSize+"/"+activo;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarZonas> type = new ParameterizedTypeReference<ResponseListarZonas>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarZonas> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarZonas response = lista.getBody();
	
		return response;
	}
	
	public ResponseInsertarZona insertarZona(Zona zona) {
		String url = urlBaseAdminConfigApi+"Zona/InsertarZona";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseInsertarZona> type = new ParameterizedTypeReference<ResponseInsertarZona>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Zona> entity = new HttpEntity<>(zona,headers);
		
		ResponseEntity<ResponseInsertarZona> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseInsertarZona response = dep.getBody();
		
		return response;
	}
	
	public ResponseSeleccionarZona seleccionarZona(Integer zona_Id) {
		String url = urlBaseMapaApi+"Zona/SeleccionarZona/"+zona_Id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarZona> type = new ParameterizedTypeReference<ResponseSeleccionarZona>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarZona> zona = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarZona response = zona.getBody();
				
		return response;
	}
	
	public ResponseInsertarZona actualizarZona(Zona zona) {
		String url = urlBaseAdminConfigApi+"Zona/ActualizarZona";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseInsertarZona> type = new ParameterizedTypeReference<ResponseInsertarZona>() {};	
		
		
		HttpEntity<Zona> entity = new HttpEntity<Zona>(zona, headers);
		
		ResponseEntity<ResponseInsertarZona> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseInsertarZona response = dep.getBody();

		return response;
	}
	
	public ResponseEliminarZona eliminarZona(Integer id) {
		String url = urlBaseAdminConfigApi+"Zona/EliminarZona/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseEliminarZona> type = new ParameterizedTypeReference<ResponseEliminarZona>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseEliminarZona> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseEliminarZona response = dep.getBody();
		
		return response;
	}
	
}
