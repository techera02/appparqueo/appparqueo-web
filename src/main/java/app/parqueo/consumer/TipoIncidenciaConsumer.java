package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.supervision.ResponseListarTipoIncidencias;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class TipoIncidenciaConsumer {

	@Value("${supervision-api.url.base}")
	private String urlBaseSupervisionApi;

	public ResponseListarTipoIncidencias ListarTipoIncidencias() {
		String url = urlBaseSupervisionApi + "TipoIncidencia/ListarTipoIncidencias";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarTipoIncidencias>
		type = new ParameterizedTypeReference<ResponseListarTipoIncidencias>() {
		};

		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarTipoIncidencias> lista = restTemplate.exchange(url, HttpMethod.GET, entity, type);

		ResponseListarTipoIncidencias response = lista.getBody();

		return response;
	}

}
