package app.parqueo.consumer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminseguridad.ResponseListarPerfiles;
import app.parqueo.model.adminseguridad.ResponsePerfil;
import app.parqueo.model.adminseguridad.ResponsePerfilPaginador;
import app.parqueo.model.adminseguridad.Perfil;
import app.parqueo.model.adminseguridad.ResponseInsertarPerfil;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class PerfilConsumer {
	
	@Value("${adminseguridad-api.url.base}")
	private String urlBaseAdminseguridadApi;
	
	public List<ResponsePerfilPaginador> listarPerfilesCB(){
		
		Integer rowCount = this.listarPerfiles(1,1,"null",0,0).getLista().get(0).getRowCount();
		
		String url = urlBaseAdminseguridadApi+"Perfil/ListarPerfiles/1/"+rowCount+"/null/0/0";
					
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarPerfiles> type = new ParameterizedTypeReference<ResponseListarPerfiles>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarPerfiles> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarPerfiles response = lista.getBody();
		
		List<ResponsePerfilPaginador> salida = new ArrayList<ResponsePerfilPaginador>();
		
		for(int i=0;i<response.getLista().size();i++) {
			if(response.getLista().get(i).getPerf_Activo() == true) {
				salida.add(response.getLista().get(i));
			}
		}
			
		return salida;
	}
	
	public ResponsePerfil seleccionarPerfil(Integer id) {
		String url = urlBaseAdminseguridadApi+"Perfil/SeleccionarPerfil/"+id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponsePerfil> type = new ParameterizedTypeReference<ResponsePerfil>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponsePerfil> usuario = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponsePerfil response = usuario.getBody();
		
		return response;
	}
	
	public ResponseListarPerfiles listarPerfiles(Integer pageNumber,Integer pageSize,String nombre, Integer activo,
			Integer indiceNombre){
		String url = urlBaseAdminseguridadApi+"Perfil/ListarPerfiles/"+pageNumber+"/"+pageSize+"/"+nombre+"/"+activo+"/"+indiceNombre;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarPerfiles> type = new ParameterizedTypeReference<ResponseListarPerfiles>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarPerfiles> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarPerfiles response = lista.getBody();
					
		return response;
	}
	
	public ResponseInsertarPerfil insertarPerfil(Perfil perfil) {
		String url = urlBaseAdminseguridadApi+"Perfil/InsertarPerfil";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseInsertarPerfil> type = new ParameterizedTypeReference<ResponseInsertarPerfil>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Perfil> entity = new HttpEntity<>(perfil,headers);
		
		ResponseEntity<ResponseInsertarPerfil> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseInsertarPerfil response = dep.getBody();
		
		return response;
	}
	
	public ResponsePerfil actualizarPerfil(Perfil perfil) {
		String url = urlBaseAdminseguridadApi+"Perfil/ActualizarPerfil";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponsePerfil> type = new ParameterizedTypeReference<ResponsePerfil>() {};	
		
		
		HttpEntity<Perfil> entity = new HttpEntity<Perfil>(perfil, headers);
		
		ResponseEntity<ResponsePerfil> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponsePerfil response = dep.getBody();

		return response;
	}
	
	public ResponsePerfil eliminarPerfil(Integer id) {
		String url = urlBaseAdminseguridadApi+"Perfil/EliminarPerfil/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponsePerfil> type = new ParameterizedTypeReference<ResponsePerfil>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponsePerfil> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponsePerfil response = dep.getBody();
		
		return response;
	}
}
