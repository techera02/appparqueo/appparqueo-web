package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminmaestros.ResponseListarSupervisores;
import app.parqueo.model.adminmaestros.ResponseReenviarCredencialesSupervisor;
import app.parqueo.model.adminmaestros.ResponseSeleccionarSupervisor;
import app.parqueo.model.adminmaestros.ResponseSupervisor;
import app.parqueo.model.adminmaestros.Supervisor;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class SupervisorConsumer {

	@Value("${adminmaestros-api.url.base}")
	private String urlBaseAdminMaestrosApi;
	
	@Value("${supervision-api.url.base}")
	private String urlBaseSupervisionApi;
	
	public ResponseListarSupervisores listarSupervisores(Integer pageNumber,Integer pageSize, String nombre, String documento,
			Integer activo){
		String url = urlBaseAdminMaestrosApi+"Supervisor/ListarSupervisor/"
	+pageNumber+"/"+pageSize+"/"+nombre+"/"+documento+"/"+activo;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarSupervisores> type = new ParameterizedTypeReference<ResponseListarSupervisores>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarSupervisores> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarSupervisores response = lista.getBody();
	
		return response;
	}
	
	public ResponseSupervisor insertarSupervisor(Supervisor supervisor) {
		String url = urlBaseAdminMaestrosApi+"Supervisor/InsertarSupervisor";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseSupervisor> type = new ParameterizedTypeReference<ResponseSupervisor>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Supervisor> entity = new HttpEntity<>(supervisor,headers);
		
		ResponseEntity<ResponseSupervisor> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseSupervisor response = dep.getBody();
		
		return response;
	}
	
	public ResponseSupervisor actualizarSupervisor(Supervisor supervisor) {
		String url = urlBaseAdminMaestrosApi+"Supervisor/ActualizarSupervisor";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseSupervisor> type = new ParameterizedTypeReference<ResponseSupervisor>() {};	
		
		
		HttpEntity<Supervisor> entity = new HttpEntity<Supervisor>(supervisor, headers);
		
		ResponseEntity<ResponseSupervisor> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseSupervisor response = dep.getBody();

		return response;
	}
	
	public Supervisor seleccionarSupervisor(Integer id) {
		String url = urlBaseSupervisionApi+"Supervisor/SeleccionarSupervisor/"+id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarSupervisor> type = new ParameterizedTypeReference<ResponseSeleccionarSupervisor>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarSupervisor> supervisor = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarSupervisor response = supervisor.getBody();
		
		return response.getSupervisor();
	}
	
	public ResponseSupervisor eliminarSupervisor(Integer id) {
		String url = urlBaseAdminMaestrosApi+"Supervisor/EliminarSupervisor/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseSupervisor> type = new ParameterizedTypeReference<ResponseSupervisor>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSupervisor> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseSupervisor response = dep.getBody();
		
		return response;
	}
	
	public ResponseReenviarCredencialesSupervisor reenviarCredencialesSupervisor(Integer id) {
		String url = urlBaseAdminMaestrosApi+"Supervisor/ReenviarCredencialesSupervisor/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseReenviarCredencialesSupervisor> type = new ParameterizedTypeReference<ResponseReenviarCredencialesSupervisor>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseReenviarCredencialesSupervisor> dep = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		ResponseReenviarCredencialesSupervisor response = dep.getBody();
		
		return response;
	}
}
