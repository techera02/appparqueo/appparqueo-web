package app.parqueo.consumer;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminmaestros.ResponseListarTipoDocumentos;
import app.parqueo.model.adminmaestros.TipoDocumento;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class TipoDocumentoConsumer {

	@Value("${adminmaestros-api.url.base}")
	private String urlBaseAdminMaestrosApi;
	
	public List<TipoDocumento> listarTipoDocumentos(){
		String url = urlBaseAdminMaestrosApi+"TipoDocumento/ListarTipoDocumento";
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarTipoDocumentos> type = new ParameterizedTypeReference<ResponseListarTipoDocumentos>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarTipoDocumentos> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarTipoDocumentos response = lista.getBody();
	
		return response.getLista();
	}
}
