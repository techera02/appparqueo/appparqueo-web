package app.parqueo.consumer;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminseguridad.PerfilMenu;
import app.parqueo.model.adminseguridad.ResponseInsertarConfiguracionPerfil;
import app.parqueo.model.adminseguridad.ResponseListarAccesosMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseListarConfiguracionMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseSeleccionarAccesosPorMenu;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class PerfilMenuConsumer {
	
	@Value("${adminseguridad-api.url.base}")
	private String urlBaseAdminseguridadApi;

	public ResponseListarAccesosMenuPorPerfil listarAccesosMenuPorPerfil(Integer idperfil) {
		
		String url = urlBaseAdminseguridadApi+"PerfilMenu/ListarAccesosMenuPorPerfil/"+idperfil;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarAccesosMenuPorPerfil> type = new ParameterizedTypeReference<ResponseListarAccesosMenuPorPerfil>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarAccesosMenuPorPerfil> result = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarAccesosMenuPorPerfil response = result.getBody();
					
		return response;	
	}
	
	public ResponseSeleccionarAccesosPorMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu ) {
		String url = urlBaseAdminseguridadApi+"PerfilMenu/SeleccionarAccesosPorMenu/"+idperfil+"/"+idmenu;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseSeleccionarAccesosPorMenu> type = new ParameterizedTypeReference<ResponseSeleccionarAccesosPorMenu>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarAccesosPorMenu> accesos = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarAccesosPorMenu response = accesos.getBody();
		
		return response;
	}
	
	public ResponseListarConfiguracionMenuPorPerfil listarConfiguracionMenuPorPerfil(Integer idperfil) {
		
		String url = urlBaseAdminseguridadApi+"PerfilMenu/ListarConfiguracionMenuPorPerfil/"+idperfil;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseListarConfiguracionMenuPorPerfil> type 
		= new ParameterizedTypeReference<ResponseListarConfiguracionMenuPorPerfil>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarConfiguracionMenuPorPerfil> result = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarConfiguracionMenuPorPerfil response = result.getBody();
					
		return response;	
	}
	public ResponseInsertarConfiguracionPerfil insertarConfiguracionPerfil(List<PerfilMenu> lista) {
		String url = urlBaseAdminseguridadApi+"PerfilMenu/InsertarConfiguracionPerfil";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseInsertarConfiguracionPerfil> type
		= new ParameterizedTypeReference<ResponseInsertarConfiguracionPerfil>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<List<PerfilMenu>> entity = new HttpEntity<>(lista,headers);
		
		ResponseEntity<ResponseInsertarConfiguracionPerfil> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseInsertarConfiguracionPerfil response = dep.getBody();
		
		return response;
	}
}
