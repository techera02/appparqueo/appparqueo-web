package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminconfig.ResponseActualizarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseEliminarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseInsertarEstacionamiento;
import app.parqueo.model.mapa.Estacionamiento;
import app.parqueo.model.adminconfig.ResponseListarEstacionamientosPorZona;
import app.parqueo.model.mapa.ResponseSeleccionarEstacionamiento;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class EstacionamientoConsumer {
	
	@Value("${mapa-api.url.base}")
	private String urlBaseMapaApi;
	
	@Value("${adminconfig-api.url.base}")
	private String urlBaseAdminConfigApi;
	
	public ResponseListarEstacionamientosPorZona listarEstacionamientosPorZona(Integer idZona, Integer pagenumber, 
			Integer pagesize,  String nombre, Integer indicenombre, String codigo, Integer indicecodigo){
		String url = urlBaseAdminConfigApi+"Estacionamiento/ListarEstacionamientosPorZona/"+idZona+"/"+pagenumber+"/"+pagesize+
				"/"+nombre+"/"+indicenombre+"/"+codigo+"/"+indicecodigo;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarEstacionamientosPorZona>
		type = new ParameterizedTypeReference<ResponseListarEstacionamientosPorZona>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarEstacionamientosPorZona> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarEstacionamientosPorZona response = lista.getBody();
	
		return response;
	}
	
	public ResponseInsertarEstacionamiento insertarEstacionamiento(Estacionamiento estacionamiento) {
		String url = urlBaseAdminConfigApi+"Estacionamiento/InsertarEstacionamiento";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseInsertarEstacionamiento> type =
				new ParameterizedTypeReference<ResponseInsertarEstacionamiento>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Estacionamiento> entity = new HttpEntity<>(estacionamiento,headers);
		
		ResponseEntity<ResponseInsertarEstacionamiento> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseInsertarEstacionamiento response = dep.getBody();
		
		return response;
	}
	
	public ResponseSeleccionarEstacionamiento seleccionarEstacionamiento(Integer idEstacionamiento) {
		String url = urlBaseMapaApi+"Estacionamiento/SeleccionarEstacionamiento/"+idEstacionamiento;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarEstacionamiento> type = 
				new ParameterizedTypeReference<ResponseSeleccionarEstacionamiento>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarEstacionamiento> dep = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarEstacionamiento response = dep.getBody();
		
		return response;
	}
	
	public ResponseActualizarEstacionamiento actualizarEstacionamiento(Estacionamiento estacionamiento) {
		String url = urlBaseAdminConfigApi+"Estacionamiento/ActualizarEstacionamiento";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseActualizarEstacionamiento> type =
				new ParameterizedTypeReference<ResponseActualizarEstacionamiento>() {};	
		
		
		HttpEntity<Estacionamiento> entity = new HttpEntity<Estacionamiento>(estacionamiento, headers);
		
		ResponseEntity<ResponseActualizarEstacionamiento> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseActualizarEstacionamiento response = dep.getBody();
	
		return response;
	}
	public ResponseEliminarEstacionamiento eliminarEstacionamiento(Integer idestacionamiento) {
		String url = urlBaseAdminConfigApi+"Estacionamiento/EliminarEstacionamiento/"+idestacionamiento;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseEliminarEstacionamiento> type =
				new ParameterizedTypeReference<ResponseEliminarEstacionamiento>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseEliminarEstacionamiento> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseEliminarEstacionamiento response = dep.getBody();
		
		return response;
	
	}
}
