package app.parqueo.consumer;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminmaestros.ResponseListarIncidencia;
import app.parqueo.model.adminmaestros.ResponseSeleccionarIncidencia;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class IncidenciaConsumer {

	@Value("${adminmaestros-api.url.base}")
	private String urlBaseAdminMaestrosApi;
	
	public ResponseListarIncidencia listarIncidencias(Integer pageNumber,Integer pageSize, Date fechainicio,
			Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin, Integer tipoincidencia, String placa, Integer indicePlaca){
		
		String url = urlBaseAdminMaestrosApi+"Incidencia/ListarIncidencia/"+pageNumber+"/"+pageSize+"/"+fechainicio+
				"/"+indiceFechainicio+"/"+fechafin+"/"+indiceFechaFin+"/"+tipoincidencia+"/"+placa+"/"+indicePlaca;
	     
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarIncidencia> type = new ParameterizedTypeReference<ResponseListarIncidencia>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarIncidencia> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarIncidencia response = lista.getBody();
		return response;
	}
	
	public ResponseSeleccionarIncidencia seleccionarIncidencia(Integer idincidencia) {
		String url = urlBaseAdminMaestrosApi+"Incidencia/SeleccionarIncidencia/"+idincidencia;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarIncidencia> type = new ParameterizedTypeReference<ResponseSeleccionarIncidencia>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarIncidencia> incidencia = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseSeleccionarIncidencia response = incidencia.getBody();
		return response;
	}
}
