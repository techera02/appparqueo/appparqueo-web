package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminmaestros.ResponseListarCliente;
import app.parqueo.model.adminmaestros.ResponseSeleccionarDetalleCliente;
import app.parqueo.model.cliente.ResponseListarVehiculosPorCliente;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class ClienteConsumer {
	
	@Value("${adminmaestros-api.url.base}")
	private String urlBaseAdminMaestrosApi;
	
	@Value("${cliente-api.url.base}")
	private String urlBaseClienteApi;
	
	public ResponseListarCliente listarCliente(Integer pageNumber, Integer pageSize, 
			String nombre, Integer indiceNombre, String nrodocumento, Integer indiceNrodocumento,
			String placa, Integer indicePlaca){
		String url = urlBaseAdminMaestrosApi+"Cliente/ListarCliente/"+pageNumber+"/"+pageSize+"/"+nombre+
				"/"+indiceNombre+"/"+nrodocumento+"/"+indiceNrodocumento+"/"+placa+"/"+indicePlaca;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarCliente> type = new ParameterizedTypeReference<ResponseListarCliente>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarCliente> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarCliente response = lista.getBody();
					
		return response;
	}
	
	public ResponseSeleccionarDetalleCliente seleccionarDetalleCliente(Integer idcliente) {
		String url = urlBaseAdminMaestrosApi+"Cliente/SeleccionarDetalleCliente/"+idcliente;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarDetalleCliente> type = new ParameterizedTypeReference<ResponseSeleccionarDetalleCliente>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarDetalleCliente> cliente = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarDetalleCliente response = cliente.getBody();
		
		return response;
	}
	
	public ResponseListarVehiculosPorCliente listarVehiculosPorCliente(Integer idCliente){
		String url = urlBaseClienteApi+"Cliente/ListarVehiculosPorCliente/"+idCliente;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarVehiculosPorCliente> type = new ParameterizedTypeReference<ResponseListarVehiculosPorCliente>() {};
					
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarVehiculosPorCliente> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
					
		ResponseListarVehiculosPorCliente response = lista.getBody();
					
		return response;
	}
	
	
}
