package app.parqueo.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import app.parqueo.model.adminseguridad.ResponseListarUsuarios;
import app.parqueo.model.adminseguridad.ResponseRecuperarContraseniaAdmin;
import app.parqueo.model.adminseguridad.ResponseSeleccionarUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuarioIniciarSesion;
import app.parqueo.model.adminseguridad.Usuario;
import app.parqueo.model.adminseguridad.UsuarioLogin;

@Repository
@PropertySource("classpath:properties/AppWeb.properties")
public class UsuarioConsumer {
	
	@Value("${adminseguridad-api.url.base}")
	private String urlBaseAdminseguridadApi;

	public ResponseUsuarioIniciarSesion iniciarSesionAdmin(UsuarioLogin usuario) {
		String url = urlBaseAdminseguridadApi+"Usuario/IniciarSesionAdmin";
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseUsuarioIniciarSesion> type = new ParameterizedTypeReference<ResponseUsuarioIniciarSesion>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<UsuarioLogin> entity = new HttpEntity<>(usuario,headers);
		
		ResponseEntity<ResponseUsuarioIniciarSesion> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		
		ResponseUsuarioIniciarSesion response = dep.getBody();
		
		return response;
	}
	
	public ResponseUsuario insertarUsuario(Usuario usuario) {
		String url = urlBaseAdminseguridadApi+"Usuario/InsertarUsuario";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();

		ParameterizedTypeReference<ResponseUsuario> type = new ParameterizedTypeReference<ResponseUsuario>() {
		};
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8");
		
		HttpEntity<Usuario> entity = new HttpEntity<>(usuario,headers);
		
		ResponseEntity<ResponseUsuario> dep = restTemplate
				.exchange(url, HttpMethod.POST, entity, type);
		ResponseUsuario response = dep.getBody();
		
		return response;
	}
	
	public ResponseUsuario actualizarUsuario(Usuario usuario) {
		String url = urlBaseAdminseguridadApi+"Usuario/ActualizarUsuario";

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ParameterizedTypeReference<ResponseUsuario> type = new ParameterizedTypeReference<ResponseUsuario>() {};	
		
		
		HttpEntity<Usuario> entity = new HttpEntity<Usuario>(usuario, headers);
		
		ResponseEntity<ResponseUsuario> dep = restTemplate.exchange(url, HttpMethod.PUT, entity, type);
		
		ResponseUsuario response = dep.getBody();

		return response;
	}
	
	public Usuario seleccionarUsuario(Integer id) {
		String url = urlBaseAdminseguridadApi+"Usuario/SeleccionarUsuario/"+id;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarUsuario> type = new ParameterizedTypeReference<ResponseSeleccionarUsuario>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarUsuario> usuario = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarUsuario response = usuario.getBody();
		
		return response.getUsuario();
	}
	
	public ResponseUsuario eliminarUsuario(Integer id) {
		String url = urlBaseAdminseguridadApi+"Usuario/EliminarUsuario/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseUsuario> type = new ParameterizedTypeReference<ResponseUsuario>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseUsuario> dep = restTemplate
				.exchange(url, HttpMethod.DELETE, entity, type);
		ResponseUsuario response = dep.getBody();
		
		return response;
	}

	public ResponseListarUsuarios listarUsuarios(Integer pageNumber,Integer pageSize, String nombre, Integer idperfil,
			Integer activo, Integer indicadorNombre){
		String url = urlBaseAdminseguridadApi+"Usuario/ListarUsuarios/"
	+pageNumber+"/"+pageSize+"/"+nombre+"/"+idperfil+"/"+activo+"/"+indicadorNombre;	
		
		if(nombre == null) {
			url = urlBaseAdminseguridadApi+"Usuario/ListarUsuariosFiltro/"
					+pageNumber+"/"+pageSize+"/null";	
		}
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseListarUsuarios> type = new ParameterizedTypeReference<ResponseListarUsuarios>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseListarUsuarios> lista = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseListarUsuarios response = lista.getBody();
	
		return response;
	}
	
	public ResponseRecuperarContraseniaAdmin reenviarCredencialesUsuario(Integer id) {
		String url = urlBaseAdminseguridadApi+"Usuario/ReenviarCredencialesUsuario/"+id;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		ParameterizedTypeReference<ResponseRecuperarContraseniaAdmin> 
		type = new ParameterizedTypeReference<ResponseRecuperarContraseniaAdmin>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseRecuperarContraseniaAdmin> dep = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseRecuperarContraseniaAdmin response = dep.getBody();
		
		return response;
	}
	
	
	public ResponseSeleccionarUsuario seleccionarUsuarioCorreo(String correo) {
		String url = urlBaseAdminseguridadApi+"Usuario/SeleccionarUsuarioCorreo/"+correo;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		ParameterizedTypeReference<ResponseSeleccionarUsuario> type = new ParameterizedTypeReference<ResponseSeleccionarUsuario>() {
		};
		
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		ResponseEntity<ResponseSeleccionarUsuario> usuario = restTemplate
				.exchange(url, HttpMethod.GET, entity, type);
		
		ResponseSeleccionarUsuario response = usuario.getBody();
		
		return response;
	}
	
}
