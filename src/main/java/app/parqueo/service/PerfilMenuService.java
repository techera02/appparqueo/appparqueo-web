package app.parqueo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminseguridad.PerfilMenu;
import app.parqueo.model.adminseguridad.ResponseInsertarConfiguracionPerfil;
import app.parqueo.model.adminseguridad.ResponseListarAccesosMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseListarConfiguracionMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseSeleccionarAccesosPorMenu;

@Service
public interface PerfilMenuService {
	public ResponseListarAccesosMenuPorPerfil listarAccesosMenuPorPerfil(Integer idperfil);
	public ResponseSeleccionarAccesosPorMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu );
	public ResponseListarConfiguracionMenuPorPerfil listarConfiguracionMenuPorPerfil (Integer idperfil);
	public ResponseInsertarConfiguracionPerfil insertarConfiguracionPerfil (List<PerfilMenu> lista);
}
