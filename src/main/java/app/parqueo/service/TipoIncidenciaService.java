package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.supervision.ResponseListarTipoIncidencias;

@Service
public interface TipoIncidenciaService {
	public ResponseListarTipoIncidencias ListarTipoIncidencias();
}
