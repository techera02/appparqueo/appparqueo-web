package app.parqueo.service;

import java.sql.Date;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminreportes.ResponseReporteDeParqueos;

@Service
public interface AdminReportesService {
	
	public ResponseReporteDeParqueos reporteDeParqueosZona(Integer pageNumber, Integer pageSize,  Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin);
	
	
}
