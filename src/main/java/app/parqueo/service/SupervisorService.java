package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminmaestros.ResponseListarSupervisores;
import app.parqueo.model.adminmaestros.ResponseReenviarCredencialesSupervisor;
import app.parqueo.model.adminmaestros.ResponseSupervisor;
import app.parqueo.model.adminmaestros.Supervisor;

@Service
public interface SupervisorService {

	public ResponseListarSupervisores listarSupervisores(Integer pageNumber, Integer pageSize, String nombre,
			String documento, Integer activo);

	public ResponseSupervisor insertarSupervisor(Supervisor supervisor);

	public Supervisor seleccionarSupervisor(Integer id);

	public ResponseSupervisor actualizarSupervisor(Supervisor supervisor);

	public ResponseSupervisor eliminarSupervisor(Integer id);
	
	public ResponseReenviarCredencialesSupervisor reenviarCredencialesSupervisor(Integer id);
}