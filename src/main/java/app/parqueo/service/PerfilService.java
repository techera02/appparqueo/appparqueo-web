package app.parqueo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminseguridad.ResponsePerfil;
import app.parqueo.model.adminseguridad.ResponsePerfilPaginador;
import app.parqueo.model.adminseguridad.Perfil;
import app.parqueo.model.adminseguridad.ResponseInsertarPerfil;
import app.parqueo.model.adminseguridad.ResponseListarPerfiles;

@Service
public interface PerfilService {
	public List<ResponsePerfilPaginador> listarPerfilesCB();
	
	public ResponseListarPerfiles listarPerfiles(Integer pageNumber,Integer pageSize, String nombre, Integer activo,
			Integer indiceNombre);
	
	public ResponsePerfil seleccionarPerfil(Integer id);
	
	public ResponseInsertarPerfil insertarPerfil(Perfil perfil);
	
	public ResponsePerfil actualizarPerfil(Perfil perfil);
	
	public ResponsePerfil eliminarPerfil(Integer id);

}
