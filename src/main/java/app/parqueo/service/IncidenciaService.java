package app.parqueo.service;

import java.sql.Date;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminmaestros.ResponseListarIncidencia;
import app.parqueo.model.adminmaestros.ResponseSeleccionarIncidencia;

@Service
public interface IncidenciaService {
	public ResponseListarIncidencia listarIncidencias(Integer pageNumber,Integer pageSize, Date fechainicio,
			Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin, Integer tipoincidencia, String placa, Integer indicePlaca);
	public ResponseSeleccionarIncidencia seleccionarIncidencia(Integer idincidencia);	
}
