package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminconfig.ResponseActualizarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseEliminarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseInsertarEstacionamiento;
import app.parqueo.model.mapa.Estacionamiento;
import app.parqueo.model.adminconfig.ResponseListarEstacionamientosPorZona;
import app.parqueo.model.mapa.ResponseSeleccionarEstacionamiento;

@Service
public interface EstacionamientoService {
	public ResponseListarEstacionamientosPorZona listarEstacionamientosPorZona(Integer idZona, Integer pagenumber, 
			Integer pagesize, String nombre, Integer indicenombre, String codigo, Integer indicecodigo);
	
    public ResponseInsertarEstacionamiento insertarEstacionamiento(Estacionamiento estacionamiento);

    public ResponseSeleccionarEstacionamiento seleccionarEstacionamiento(Integer idEstacionamiento);
    
    public ResponseActualizarEstacionamiento actualizarEstacionamiento(Estacionamiento estacionamiento);
    
    public ResponseEliminarEstacionamiento eliminarEstacionamiento(Integer idestacionamiento);

}
