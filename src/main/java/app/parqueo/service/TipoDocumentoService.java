package app.parqueo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminmaestros.TipoDocumento;

@Service
public interface TipoDocumentoService {

	public List<TipoDocumento> listarTipoDocumentos();
}
