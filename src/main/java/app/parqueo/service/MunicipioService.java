package app.parqueo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminmaestros.Municipio;
import app.parqueo.model.adminmaestros.ResponseListarMunicipios;
import app.parqueo.model.adminmaestros.ResponseMunicipio;
import app.parqueo.model.adminmaestros.ResponseMunicipioPaginador;

@Service
public interface MunicipioService {

	public ResponseListarMunicipios listarMunicipios(Integer pageNumber,Integer pageSize, String nombre, Integer activo);
		
	public List<ResponseMunicipioPaginador> listarMunicipiosCB();
	
	public ResponseMunicipio insertarMunicipio(Municipio municipio);
	
	public Municipio seleccionarMunicipio(Integer id);

	public ResponseMunicipio actualizarMunicipio(Municipio municipio);
	
	public ResponseMunicipio eliminarMunicipio(Integer idmunicipio, Integer idtarifa);
}
