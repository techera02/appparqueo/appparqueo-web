package app.parqueo.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import app.parqueo.model.adminseguridad.ResponseListarUsuarios;
import app.parqueo.model.adminseguridad.ResponseRecuperarContraseniaAdmin;
import app.parqueo.model.adminseguridad.ResponseSeleccionarUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuarioIniciarSesion;
import app.parqueo.model.adminseguridad.Usuario;
import app.parqueo.model.adminseguridad.UsuarioLogin;

@Service
public interface UsuarioService extends UserDetailsService{
	
	public ResponseUsuarioIniciarSesion iniciarSesionAdmin(UsuarioLogin usuario);
	
	public ResponseUsuario insertarUsuario(Usuario usuario);
	
	public Usuario seleccionarUsuario(Integer id);

	public ResponseUsuario actualizarUsuario(Usuario usuario);
	
	public ResponseUsuario eliminarUsuario(Integer id);
		
	public ResponseListarUsuarios listarUsuarios(Integer pageNumber,Integer pageSize, String nombre, Integer idperfil,
			Integer activo,  Integer indicadorNombre);
	
	public ResponseRecuperarContraseniaAdmin reenviarCredencialesUsuario(Integer id);
	
	public ResponseSeleccionarUsuario seleccionarUsuarioCorreo(String correo);

}
