package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminmaestros.ResponseListarCliente;
import app.parqueo.model.adminmaestros.ResponseSeleccionarDetalleCliente;
import app.parqueo.model.cliente.ResponseListarVehiculosPorCliente;

@Service
public interface ClienteService {
	public ResponseListarCliente listarClientes( Integer pageNumber, Integer pageSize, 
			String nombre, Integer indiceNombre, String nrodocumento, Integer indiceNrodocumento,
			String placa, Integer indicePlaca);
	
	public ResponseSeleccionarDetalleCliente seleccionarDetalleCliente(Integer idcliente);
	
	public ResponseListarVehiculosPorCliente listarVehiculosPorCliente(Integer idCliente);
}
