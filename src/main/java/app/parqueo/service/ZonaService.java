package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminconfig.ResponseEliminarZona;
import app.parqueo.model.adminconfig.ResponseInsertarZona;
import app.parqueo.model.adminconfig.ResponseListarZonas;
import app.parqueo.model.adminconfig.Zona;
import app.parqueo.model.mapa.ResponseSeleccionarZona;

@Service
public interface ZonaService {
	public ResponseListarZonas listarZonas(Integer Muni_Id, Integer pageNumber,Integer pageSize, Integer activo);
	
	public ResponseInsertarZona insertarZona(Zona zona);
	
	public ResponseInsertarZona actualizarZona(Zona zona);
	
	public ResponseSeleccionarZona seleccionarZona(Integer zona_Id);
	
	public ResponseEliminarZona eliminarZona(Integer zona_Id);
}
