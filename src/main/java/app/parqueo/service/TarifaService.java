package app.parqueo.service;

import org.springframework.stereotype.Service;

import app.parqueo.model.adminconfig.ResponseListarTarifas;
import app.parqueo.model.adminconfig.ResponseTarifa;
import app.parqueo.model.adminconfig.Tarifa;

@Service
public interface TarifaService {

	public ResponseListarTarifas listarTarifas(Integer pageNumber,Integer pageSize);
	
	public ResponseTarifa insertarTarifa(Tarifa tarifa);
	
	public Tarifa seleccionarTarifa(Integer id);

	public ResponseTarifa actualizarTarifa(Tarifa tarifa);
	
	public ResponseTarifa eliminarTarifa(Integer id);
	
	public Tarifa seleccionarTarifaPorMunicipio(Integer idmunicipio);
}
