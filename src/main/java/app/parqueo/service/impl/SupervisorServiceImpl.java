package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.SupervisorConsumer;
import app.parqueo.model.adminmaestros.ResponseListarSupervisores;
import app.parqueo.model.adminmaestros.ResponseReenviarCredencialesSupervisor;
import app.parqueo.model.adminmaestros.ResponseSupervisor;
import app.parqueo.model.adminmaestros.Supervisor;
import app.parqueo.service.SupervisorService;

@Service("SupervisorService")
public class SupervisorServiceImpl implements SupervisorService {

	@Autowired
	SupervisorConsumer supervisorConsumer;

	@Override
	public ResponseListarSupervisores listarSupervisores(Integer pageNumber, Integer pageSize, String nombre,
			String documento, Integer activo) {
		return this.supervisorConsumer.listarSupervisores(pageNumber, pageSize, nombre, documento, activo);
	}

	@Override
	public ResponseSupervisor insertarSupervisor(Supervisor supervisor) {
		return this.supervisorConsumer.insertarSupervisor(supervisor);
	}

	@Override
	public Supervisor seleccionarSupervisor(Integer id) {
		return this.supervisorConsumer.seleccionarSupervisor(id);
	}

	@Override
	public ResponseSupervisor actualizarSupervisor(Supervisor supervisor) {
		return this.supervisorConsumer.actualizarSupervisor(supervisor);
	}

	@Override
	public ResponseSupervisor eliminarSupervisor(Integer id) {
		return this.supervisorConsumer.eliminarSupervisor(id);
	}
	
	@Override
	public ResponseReenviarCredencialesSupervisor reenviarCredencialesSupervisor(Integer id) {
		return this.supervisorConsumer.reenviarCredencialesSupervisor(id);
	}
}
