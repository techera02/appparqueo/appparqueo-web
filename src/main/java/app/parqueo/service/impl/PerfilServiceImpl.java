package app.parqueo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.PerfilConsumer;
import app.parqueo.model.adminseguridad.Perfil;
import app.parqueo.model.adminseguridad.ResponseInsertarPerfil;
import app.parqueo.model.adminseguridad.ResponseListarPerfiles;
import app.parqueo.model.adminseguridad.ResponsePerfil;
import app.parqueo.model.adminseguridad.ResponsePerfilPaginador;
import app.parqueo.service.PerfilService;

@Service("PerfilService")
public class PerfilServiceImpl implements PerfilService {
	@Autowired
	PerfilConsumer perfilConsumer;
	
	@Override
	public List<ResponsePerfilPaginador> listarPerfilesCB() {
		return this.perfilConsumer.listarPerfilesCB();
	}

	@Override
	public ResponsePerfil seleccionarPerfil(Integer id) {
		return this.perfilConsumer.seleccionarPerfil(id);
	}

	@Override
	public ResponseListarPerfiles listarPerfiles(Integer pageNumber, Integer pageSize, String nombre, Integer activo,
			Integer indiceNombre) {
		return this.perfilConsumer.listarPerfiles(pageNumber, pageSize,  nombre,  activo,
				 indiceNombre);
	}

	@Override
	public ResponseInsertarPerfil insertarPerfil(Perfil perfil) {
		return this.perfilConsumer.insertarPerfil(perfil);
	}

	@Override
	public ResponsePerfil actualizarPerfil(Perfil perfil) {
		return this.perfilConsumer.actualizarPerfil(perfil);
	}

	@Override
	public ResponsePerfil eliminarPerfil(Integer id) {
		return this.perfilConsumer.eliminarPerfil(id);
	}

}
