package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.EstacionamientoConsumer;
import app.parqueo.model.adminconfig.ResponseActualizarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseEliminarEstacionamiento;
import app.parqueo.model.adminconfig.ResponseInsertarEstacionamiento;
import app.parqueo.model.mapa.Estacionamiento;
import app.parqueo.model.adminconfig.ResponseListarEstacionamientosPorZona;
import app.parqueo.model.mapa.ResponseSeleccionarEstacionamiento;
import app.parqueo.service.EstacionamientoService;

@Service("EstacionamientoService")
public class EstacionamientoServiceImpl implements EstacionamientoService{
	@Autowired
	EstacionamientoConsumer estacionamientoConsumer;
	
	@Override
	public ResponseListarEstacionamientosPorZona listarEstacionamientosPorZona(Integer idZona, Integer pagenumber, 
			Integer pagesize,  String nombre, Integer indicenombre, String codigo, Integer indicecodigo) {
		return this.estacionamientoConsumer.listarEstacionamientosPorZona(idZona,pagenumber,pagesize,
				nombre, indicenombre,codigo, indicecodigo);
	}

	@Override
	public ResponseInsertarEstacionamiento insertarEstacionamiento(Estacionamiento estacionamiento) {
		return this.estacionamientoConsumer.insertarEstacionamiento(estacionamiento);
	}

	@Override
	public ResponseSeleccionarEstacionamiento seleccionarEstacionamiento(Integer idEstacionamiento) {
		return this.estacionamientoConsumer.seleccionarEstacionamiento(idEstacionamiento);
	}

	@Override
	public ResponseActualizarEstacionamiento actualizarEstacionamiento(Estacionamiento estacionamiento) {
		return this.estacionamientoConsumer.actualizarEstacionamiento(estacionamiento);
	}

	@Override
	public ResponseEliminarEstacionamiento eliminarEstacionamiento(Integer idestacionamiento) {
		return this.estacionamientoConsumer.eliminarEstacionamiento(idestacionamiento);
	}

}
