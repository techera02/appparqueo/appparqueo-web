package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.TarifaConsumer;
import app.parqueo.model.adminconfig.ResponseListarTarifas;
import app.parqueo.model.adminconfig.ResponseTarifa;
import app.parqueo.model.adminconfig.Tarifa;
import app.parqueo.service.TarifaService;

@Service("TarifaService")
public class TarifaServiceImpl implements TarifaService{

	@Autowired
	TarifaConsumer tarifaConsumer;
	
	@Override
	public ResponseListarTarifas listarTarifas(Integer pageNumber,Integer pageSize) {
		return this.tarifaConsumer.listarTarifas( pageNumber, pageSize);
	}

	@Override
	public ResponseTarifa insertarTarifa(Tarifa tarifa) {
		return this.tarifaConsumer.insertarTarifa(tarifa);
	}

	@Override
	public Tarifa seleccionarTarifa(Integer id) {
		return this.tarifaConsumer.seleccionarTarifa(id);
	}

	@Override
	public ResponseTarifa actualizarTarifa(Tarifa tarifa) {
		return this.tarifaConsumer.actualizarTarifa(tarifa);
	}

	@Override
	public ResponseTarifa eliminarTarifa(Integer id) {
		return this.tarifaConsumer.eliminarTarifa(id);
	}
	
	@Override
	public Tarifa seleccionarTarifaPorMunicipio(Integer idmunicipio) {
		
		ResponseListarTarifas lista = this.tarifaConsumer.listarTarifas(1,0);
		
		Tarifa tarifa= new Tarifa();
		
		for(int x = 0; x<lista.getLista().size();x++) {
			if(lista.getLista().get(x).getMuni_Id() == idmunicipio) {
				tarifa.setTari_Id(lista.getLista().get(x).getTari_Id());
				tarifa.setMuni_Id(lista.getLista().get(x).getMuni_Id());
				tarifa.setTari_Monto(lista.getLista().get(x).getTari_Monto());
				break;
			}
		}
		
		return tarifa;
	}
}
