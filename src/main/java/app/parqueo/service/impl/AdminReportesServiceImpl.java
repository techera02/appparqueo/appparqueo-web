package app.parqueo.service.impl;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.AdminReportesConsumer;
import app.parqueo.model.adminreportes.ResponseReporteDeParqueos;
import app.parqueo.service.AdminReportesService;

@Service("AdminReportesService")
public class AdminReportesServiceImpl implements AdminReportesService {

	@Autowired
	AdminReportesConsumer adminReportesConsumer;
	
	@Override
	public ResponseReporteDeParqueos reporteDeParqueosZona(Integer pageNumber, Integer pageSize, Integer idmunicipio,
			Date fechainicio, Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin) {
		return this.adminReportesConsumer.reporteDeParqueosZona(pageNumber, pageSize, idmunicipio, fechainicio, indiceFechainicio, fechafin, indiceFechaFin);
	}

}
