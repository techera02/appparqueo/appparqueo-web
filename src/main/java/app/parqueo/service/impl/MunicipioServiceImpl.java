package app.parqueo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.MunicipioConsumer;
import app.parqueo.model.adminmaestros.Municipio;
import app.parqueo.model.adminmaestros.ResponseListarMunicipios;
import app.parqueo.model.adminmaestros.ResponseMunicipio;
import app.parqueo.model.adminmaestros.ResponseMunicipioPaginador;
import app.parqueo.service.MunicipioService;

@Service("MunicipioService")
public class MunicipioServiceImpl implements MunicipioService{

	@Autowired
	MunicipioConsumer municipioConsumer;
	
	@Override
	public ResponseListarMunicipios listarMunicipios(Integer pageNumber,Integer pageSize, String nombre, Integer activo) {
		return this.municipioConsumer.listarMunicipios( pageNumber, pageSize, nombre, activo);
	}

	@Override
	public ResponseMunicipio insertarMunicipio(Municipio municipio) {
		return this.municipioConsumer.insertarMunicipio(municipio);
	}

	@Override
	public Municipio seleccionarMunicipio(Integer id) {
		return this.municipioConsumer.seleccionarMunicipio(id);
	}

	@Override
	public ResponseMunicipio actualizarMunicipio(Municipio municipio) {
		return this.municipioConsumer.actualizarMunicipio(municipio);
	}

	@Override
	public ResponseMunicipio eliminarMunicipio(Integer idmunicipio, Integer idtarifa) {
		return this.municipioConsumer.eliminarMunicipio(idmunicipio, idtarifa);
	}

	@Override
	public List<ResponseMunicipioPaginador> listarMunicipiosCB() {
		return this.municipioConsumer.listarMunicipiosCB().getLista();
	}
}
