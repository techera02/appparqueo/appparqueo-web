package app.parqueo.service.impl;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.IncidenciaConsumer;
import app.parqueo.model.adminmaestros.ResponseListarIncidencia;
import app.parqueo.model.adminmaestros.ResponseSeleccionarIncidencia;
import app.parqueo.service.IncidenciaService;

@Service("IncidenciaService")
public class IncidenciaServiceImpl implements IncidenciaService{
	
	@Autowired
	IncidenciaConsumer incidenciaConsumer;
	
	@Override
	public ResponseListarIncidencia listarIncidencias(Integer pageNumber,Integer pageSize, Date fechainicio,
			Integer indiceFechainicio, Date fechafin, Integer indiceFechaFin, Integer tipoincidencia, String placa, Integer indicePlaca) {
		return this.incidenciaConsumer.listarIncidencias(pageNumber, pageSize,fechainicio,indiceFechainicio,
				fechafin, indiceFechaFin, tipoincidencia,placa,indicePlaca);
	}

	@Override
	public ResponseSeleccionarIncidencia seleccionarIncidencia(Integer idincidencia) {
		return this.incidenciaConsumer.seleccionarIncidencia(idincidencia);
	}

}
