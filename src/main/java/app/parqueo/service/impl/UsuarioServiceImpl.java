package app.parqueo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import app.parqueo.consumer.PerfilConsumer;
import app.parqueo.consumer.UsuarioConsumer;
import app.parqueo.model.adminseguridad.PerfilMenu;
import app.parqueo.model.adminseguridad.ResponseListarAccesosMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseListarUsuarios;
import app.parqueo.model.adminseguridad.ResponsePerfilMenu;
import app.parqueo.model.adminseguridad.ResponseRecuperarContraseniaAdmin;
import app.parqueo.model.adminseguridad.ResponseSeleccionarUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuario;
import app.parqueo.model.adminseguridad.ResponseUsuarioIniciarSesion;
import app.parqueo.model.adminseguridad.Usuario;
import app.parqueo.model.adminseguridad.UsuarioLogin;
import app.parqueo.service.PerfilMenuService;
import app.parqueo.service.UsuarioService;

@Service("UsuarioService")
@PropertySource("classpath:properties/AppWeb.properties")
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	UsuarioConsumer usuarioConsumer;
	
	@Autowired
	PerfilConsumer perfilConsumer;
	
	@Autowired
	PerfilMenuService perfilMenuService;
	
	@Value("${moneda}")
	private String moneda;

	@Override
	public ResponseUsuario insertarUsuario(Usuario usuario) {
		return this.usuarioConsumer.insertarUsuario(usuario);
	}

	@Override
	public Usuario seleccionarUsuario(Integer id) {
		return this.usuarioConsumer.seleccionarUsuario(id);
	}

	@Override
	public ResponseUsuario actualizarUsuario(Usuario usuario) {
		return this.usuarioConsumer.actualizarUsuario(usuario);
	}

	@Override
	public ResponseUsuario eliminarUsuario(Integer id) {
		return this.usuarioConsumer.eliminarUsuario(id);
	}

	@Override
	public ResponseUsuarioIniciarSesion iniciarSesionAdmin(UsuarioLogin usuario) {
		return this.usuarioConsumer.iniciarSesionAdmin(usuario);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpSession sesion = request.getSession(true);
		String password = request.getParameter("clave"); 
        String perf_Nombre = "Role";
          
        UsuarioLogin temp3 = new UsuarioLogin();
        temp3.setUsua_Alias(username);
        temp3.setUsua_Clave(password);
        
        ResponseUsuarioIniciarSesion temp2 = this.usuarioConsumer.iniciarSesionAdmin(temp3);
        
        if(temp2.getCodError() != null) {
        	if(temp2.getCodError() == 1) {
        		sesion.setAttribute("error", temp2.getMensaje());
        	}else if (temp2.getCodError() == 2) {
       		 	sesion.setAttribute("error", temp2.getMensaje());
        	}
        }
        
        if(username.length() == 0  && password.length() == 0) {
        	sesion.invalidate();
        }
        
        UsuarioLogin temp = temp2.getUsuario();
                
        if(temp != null && temp2.getCodError() == null) {
        	temp.setUsua_Clave(password);
            sesion.setAttribute("idusuario", temp.getUsua_Id());
            sesion.setAttribute("idperfil", temp.getPerf_Id());
            sesion.setAttribute("listPerfilMenu", new ArrayList<PerfilMenu>());
                       
            perf_Nombre = this.perfilConsumer.seleccionarPerfil(temp.getPerf_Id()).getPerfil().getPerf_Nombre();
            
            ResponseListarAccesosMenuPorPerfil response = this.perfilMenuService.listarAccesosMenuPorPerfil(temp.getPerf_Id());  
            
            List<ResponsePerfilMenu> listaPerfilMenu = response.getLista();
           
            
            List<ResponsePerfilMenu> listaPadre = new  ArrayList<ResponsePerfilMenu>();
            List<ResponsePerfilMenu> listaHija = new  ArrayList<ResponsePerfilMenu>();
            
         
            for (ResponsePerfilMenu menu : listaPerfilMenu) {
            	if(menu.getMenu_PadreId() == null) {
            		if(menu.getPmen_Activo()==true) {
                		listaPadre.add(menu);
            		}
            	}else {
            		if(menu.getPmen_Activo()==true) {
                		listaHija.add(menu);
            		}
            	}
            }    
            
            char [] arr = temp.getUsua_Nombres().trim().toCharArray();
            String nombre = "";
            
            for(int i=0; i<temp.getUsua_Nombres().length();i++) {
            	if(arr[i]==' ') {
            		break;
            	}else {
            		nombre = nombre + String.valueOf(arr[i]);
            	}
            }
            
            sesion.setAttribute("listaPadre", listaPadre);
            sesion.setAttribute("listaHija", listaHija);
            sesion.setAttribute("SNombres", nombre);
            sesion.setAttribute("SApellidos", temp.getUsua_Apellidos());
            sesion.setAttribute("error", null);
            sesion.setAttribute("moneda", moneda);
        }else {
        	temp = new UsuarioLogin();
        	temp.setUsua_Alias(" ");
        	temp.setUsua_Clave(password+"error");
        }

    	GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(perf_Nombre);

    	List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
    	grantList.add(grantedAuthority);   	

    	UserDetails user = (UserDetails) new User(temp.getUsua_Alias(),temp.getUsua_Clave(), grantList);
	    
		return user;
	}

	@Override
	public ResponseListarUsuarios listarUsuarios(Integer pageNumber, Integer pageSize, String nombre, Integer idperfil,
			Integer activo, Integer indicadorNombre) {
		return this.usuarioConsumer.listarUsuarios(pageNumber, pageSize, nombre, idperfil, activo, indicadorNombre);
	}

	@Override
	public ResponseRecuperarContraseniaAdmin reenviarCredencialesUsuario(Integer id) {
		return this.usuarioConsumer.reenviarCredencialesUsuario(id);
	}

	@Override
	public ResponseSeleccionarUsuario seleccionarUsuarioCorreo(String correo) {
		return this.usuarioConsumer.seleccionarUsuarioCorreo(correo);
	}

}
