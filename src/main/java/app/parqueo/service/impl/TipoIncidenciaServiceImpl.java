package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.TipoIncidenciaConsumer;
import app.parqueo.model.supervision.ResponseListarTipoIncidencias;
import app.parqueo.service.TipoIncidenciaService;

@Service("TipoIncidenciaService")
public class TipoIncidenciaServiceImpl implements TipoIncidenciaService {
	
	@Autowired
	TipoIncidenciaConsumer tipoIncidenciaConsumer;
	
	@Override
	public ResponseListarTipoIncidencias ListarTipoIncidencias() {
		return this.tipoIncidenciaConsumer.ListarTipoIncidencias();
	}

}
