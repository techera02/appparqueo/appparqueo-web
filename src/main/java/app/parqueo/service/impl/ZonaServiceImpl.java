package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.ZonaConsumer;
import app.parqueo.model.adminconfig.ResponseEliminarZona;
import app.parqueo.model.adminconfig.ResponseInsertarZona;
import app.parqueo.model.adminconfig.ResponseListarZonas;
import app.parqueo.model.adminconfig.Zona;
import app.parqueo.model.mapa.ResponseSeleccionarZona;
import app.parqueo.service.ZonaService;

@Service("ZonaService")
public class ZonaServiceImpl implements ZonaService{
	
	@Autowired
	ZonaConsumer zonaConsumer;

	@Override
	public ResponseListarZonas listarZonas(Integer Muni_Id, Integer pageNumber, Integer pageSize, Integer activo) {
		return this.zonaConsumer.listarZonas(Muni_Id, pageNumber, pageSize, activo);
	}

	@Override
	public ResponseInsertarZona insertarZona(Zona zona) {
		return this.zonaConsumer.insertarZona(zona);
	}

	@Override
	public ResponseSeleccionarZona seleccionarZona(Integer zona_Id) {
		return this.zonaConsumer.seleccionarZona(zona_Id);
	}

	@Override
	public ResponseInsertarZona actualizarZona(Zona zona) {
		return this.zonaConsumer.actualizarZona(zona);
	}

	@Override
	public ResponseEliminarZona eliminarZona(Integer zona_Id) {
		return this.zonaConsumer.eliminarZona(zona_Id);
	}

}
