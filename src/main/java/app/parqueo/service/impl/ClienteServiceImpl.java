package app.parqueo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.ClienteConsumer;
import app.parqueo.model.adminmaestros.ResponseListarCliente;
import app.parqueo.model.adminmaestros.ResponseSeleccionarDetalleCliente;
import app.parqueo.model.cliente.ResponseListarVehiculosPorCliente;
import app.parqueo.service.ClienteService;

@Service("ClienteService")
public class ClienteServiceImpl implements ClienteService{
	
	@Autowired
	ClienteConsumer clienteConsumer;
	
	@Override
	public ResponseListarCliente listarClientes(Integer pageNumber, Integer pageSize, 
			String nombre, Integer indiceNombre, String nrodocumento, Integer indiceNrodocumento,
			String placa, Integer indicePlaca) {
		return this.clienteConsumer.listarCliente(pageNumber, pageSize, nombre,indiceNombre,
				nrodocumento, indiceNrodocumento, placa, indicePlaca);
	}

	@Override
	public ResponseSeleccionarDetalleCliente seleccionarDetalleCliente(Integer idcliente) {
		return this.clienteConsumer.seleccionarDetalleCliente(idcliente);
	}

	@Override
	public ResponseListarVehiculosPorCliente listarVehiculosPorCliente(Integer idCliente) {
		return this.clienteConsumer.listarVehiculosPorCliente(idCliente);
	}

}
