package app.parqueo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.TipoDocumentoConsumer;
import app.parqueo.model.adminmaestros.TipoDocumento;
import app.parqueo.service.TipoDocumentoService;

@Service("TipoDocumentoService")
public class TipoDocumentoServiceImpl implements TipoDocumentoService{

	@Autowired
	TipoDocumentoConsumer tipoDocumentoConsumer;
	
	@Override
	public List<TipoDocumento> listarTipoDocumentos() {
		return this.tipoDocumentoConsumer.listarTipoDocumentos();
	}
}
