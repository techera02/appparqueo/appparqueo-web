package app.parqueo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.parqueo.consumer.PerfilMenuConsumer;
import app.parqueo.model.adminseguridad.PerfilMenu;
import app.parqueo.model.adminseguridad.ResponseInsertarConfiguracionPerfil;
import app.parqueo.model.adminseguridad.ResponseListarAccesosMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseListarConfiguracionMenuPorPerfil;
import app.parqueo.model.adminseguridad.ResponseSeleccionarAccesosPorMenu;
import app.parqueo.service.PerfilMenuService;

@Service("PerfilMenuService")
public class PerfilMenuServiceImpl implements PerfilMenuService {
	
	@Autowired
	PerfilMenuConsumer perfilMenuConsumer;
	
	@Override
	public ResponseListarAccesosMenuPorPerfil listarAccesosMenuPorPerfil(Integer idperfil) {
		return this.perfilMenuConsumer.listarAccesosMenuPorPerfil(idperfil);
	}

	@Override
	public ResponseSeleccionarAccesosPorMenu seleccionarAccesosPorMenu(Integer idperfil, Integer idmenu) {
		return this.perfilMenuConsumer.seleccionarAccesosPorMenu(idperfil, idmenu);
	}

	@Override
	public ResponseListarConfiguracionMenuPorPerfil listarConfiguracionMenuPorPerfil(Integer idperfil) {
		return this.perfilMenuConsumer.listarConfiguracionMenuPorPerfil(idperfil);
	}

	@Override
	public ResponseInsertarConfiguracionPerfil insertarConfiguracionPerfil(List<PerfilMenu> lista) {
		return this.perfilMenuConsumer.insertarConfiguracionPerfil(lista);
	}

}
