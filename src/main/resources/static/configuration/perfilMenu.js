
function changeCheckBox(row){
		$('.seleccionar-todos'+row+'').change(function() {
			$('.listado'+row+' > input[type=checkbox]').prop('checked', $(this).is(':checked'));
		});
		
		$('.seleccionar'+row+'').change(function() {
			$('.row'+row+' > input[type=checkbox]').prop('checked', $(this).is(':checked'));
		});
}


function insertarPerfil(){
	var name = document.getElementById('nombrePerfil').value;
	
	if( $('#activoPerfil').prop('checked') ) {
		active = true;
	}else{
		active = false;
	}
	
	$(".errorValidation").html('');
	$(".errorServicio").html('');
		
	if(name == null || name.length ==0){
		var errorValidation = '<div class="alert alert-warning" >El nombre no debe estar vacío</div>';
		$(".errorValidation").html(errorValidation);
	}else{
		$.ajax({
		type: 'GET',
    	url: '/seguridad/insertarPerfil?nombre='+name+'&activo='+active,
        dataType: 'json',
        success: function(result) {
			
			if(result!=null){
				if(result==0){
					var errorServicio = '<div class="alert alert-danger" >El perfil ya existe</div>';
					$(".errorServicio").html(errorServicio);

				}else {
					insertarPerfilMenu(result,0);
				}
			}else{
				var errorServicio = '<div class="alert alert-danger" >Error interno al insertar el nuevo perfil</div>';
				$(".errorServicio").html(errorServicio);
			}

		}
    });
	}
}

function actualizarPerfil(){
	var name = document.getElementById('nombrePerfil').value;
	var id = document.getElementById('idPerfil').value;
			
	if( $('#activoPerfil').prop('checked') ) {
		active = true;
	}else{
		active = false;
	}
	
	$(".errorValidation").html('');
	$(".errorServicio").html('');
	

	if(name == null || name.length ==0){
		var errorValidation = '<div class="alert alert-warning" >El nombre no debe estar vacío</div>';
		$(".errorValidation").html(errorValidation);
	}else{

		$.ajax({
		type: 'GET',
    	url: '/seguridad/actualizarPerfil?nombre='+name+'&activo='+active+'&id='+id,
        dataType: 'json',
        success: function(result) {
			if(result!=null){
				if(result==0){
					var errorServicio = '<div class="alert alert-danger" >El perfil ya existe</div>';
					$(".errorServicio").html(errorServicio);

				}else {
					insertarPerfilMenu(id, 1);
				}
			}else{
				var errorServicio = '<div class="alert alert-danger" >Error interno al insertar el nuevo perfil</div>';
				$(".errorServicio").html(errorServicio);
			}

		}
    });
	}
}

function insertarPerfilMenu(idPerfil, indice){
	var insertar = false;
	var totalPerfilMenu = 11;

	for(var x=1; x<=totalPerfilMenu; x++){
		if(x==totalPerfilMenu){
			insertar = true;
		}
		
		var nameEditar = 'menu'+x+'-editar';
		var valueEditar = false;
		var nameEliminar = 'menu'+x+'-eliminar';
		var valueEliminar = false;
		var nameCrear = 'menu'+x+'-crear';
		var valueCrear = false;
		
		var nameMenu = 'menuu'+x;
		var valueMenu = false;
		var idperfilmenu = 0;
		var idmenupadre = 0;
		
		if(indice==1){
			idperfilmenu = document.getElementById('idperfilmenu'+x).value;
			idmenupadre = document.getElementById('idmenupadre'+x).value;
			
			if(!idperfilmenu){
				idperfilmenu = 0;
			}
			if(!idmenupadre){
				idmenupadre = 0;
			}
		}

		if( $('#'+nameCrear+'').prop('checked') ) {
			valueCrear = true;
		}
		if( $('#'+nameEliminar+'').prop('checked') ) {
			valueEliminar = true;
		}
		
		if( $('#'+nameEditar+'').prop('checked') ) {
			valueEditar = true;
		}
		
		if($('#'+nameMenu+'').prop('checked') ){
			valueMenu = true;
		}
		
		 $.ajax({
            url: '/seguridad/insertarPerfilMenu?activo='+valueMenu+'&pmenid='+idperfilmenu
            +'&crear='+valueCrear+'&editar='+valueEditar+'&eliminar='+valueEliminar+
            '&menuId='+x+'&idPerfil='+idPerfil+'&insertar='+insertar+'&idmenupadre='+idmenupadre,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
				
			}
        });
       
	}
	
	location.href = "/seguridad/perfil";
}