

function recuperar(){
	var correo = document.getElementById('correo').value;
	var mensaje = '';
	
	$("#mensaje").html(mensaje);
	$("#mensaje2").html(mensaje);
	
	if(!correo){
		correo = null;
		mensaje = '<div class="alert alert-warning alert-dismissible fade show" role="alert" >'+
				'El correo electrónico no debe estar vacío'+
				'</div>';
		$("#mensaje").html(mensaje);
			
	}else{
		
	 if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(correo)){
	  
		mensaje = '<div class="alert alert-info alert-dismissible fade show" role="alert" >'+
							'Esto puede tardar unos segundos...'+
							'</div>';
					$("#mensaje2").html(mensaje);
					
			 $.ajax({
				contentType: 'application/json',
				    url: '/recuperar/RecuperarCredencialesUsuario?correo='+correo,
				    type: 'GET',
				    dataType: 'json',
				    success: function(result) {
			
						if(result.length == 0){
							mensaje = '<div class="alert alert-info alert-dismissible fade show" role="alert" >'+
									'Se ha reenviado las credenciales correctamente<br><a href="/login">Inicie sesión</a>'+
									'</div>';
							$("#mensaje2").html(mensaje);
						}else{
							mensaje = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
									result+
									'</div>';
							$("#mensaje2").html(mensaje);
						}
		
					},  error: function (jqXHR, textStatus, errorThrown) { 
							mensaje = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
									'Error del servidor, inténtalo más tarde'+
									'</div>';
							$("#mensaje2").html(mensaje);
				 	}
					
					});
	  } else {
	 	 	mensaje = '<div class="alert alert-warning alert-dismissible fade show" role="alert" >'+
				'Debe ser una dirección de correo electrónico con formato correcto'+
				'</div>';
			$("#mensaje").html(mensaje);
			
	}
		
	}
}
