
$(document).ready(function() {
    parqueosZona(1, true);
     $("#textBoton").html('Filtrar');
});
    
    
function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}
function filtrarDatosEnter(){
	
	tecla = event.keyCode;
	
	if(tecla == 13){      
       parqueosZona(1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;

function parqueosZona(page, reiniciar){
	 var contenido = '';
	 var contenidoPaginador = '';
	 var contenidoTotalRegistros = '';
	 var pageCount = 0;
	 var rowCount = 0;
	
	 var indiceFechainicio = 1;
	 var indiceFechaFin = 1;
	 
     if(reiniciar == true){
	 	var fechainicio = null;
		var fechafin = null;
		var idmunicipio = 1;
		
		indiceFechainicio = 0;
		indiceFechaFin = 0;
		
		document.getElementById("fechainicio").value = "";
		document.getElementById("fechafin").value = "";
		 $(".idmunicipio").val(1);


	}else{
		var fechainicio = document.getElementById('fechainicio').value;
		var fechafin = document.getElementById('fechafin').value;
		var idmunicipio = $(".idmunicipio").val();
				
		if(!fechainicio){
			fechainicio = null;
			indiceFechainicio = 0;
		}
		if(!fechafin){
			fechafin = null;
			indiceFechaFin = 0;
		}
		if(!idmunicipio){
			idmunicipio = 0;
		}
		
	} 
	
	var today = new Date();
	var dd = String(today.getDate()).padStart(2, '0');
	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	var yyyy = today.getFullYear();
		
	if(Date.parse(fechafin) < Date.parse(fechainicio)) {
		Swal.fire({
					  icon: 'error',
					  title: 'La fecha final debe ser mayor que la fecha inicial',
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					});
	}else if(Date.parse(fechafin) > Date.parse(yyyy+"-"+mm+"-"+dd)){
		Swal.fire({
					  icon: 'error',
					  title: 'La fecha final debe no debe ser mayor que la fecha actual',
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					});
		
	}else{
	if(page >=1 ){
		var moneda = document.getElementById('sesion_modena').value;
	 $.ajax({
		contentType: 'application/json',
		    url: '/parqueo/reporteParqueosZonaJSON?page='+page+'&fechainicio='+fechainicio+'&indiceFechainicio='+indiceFechainicio+
		    '&fechafin='+fechafin+'&indiceFechaFin='+indiceFechaFin+'&idmunicipio='+idmunicipio,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
				if(result.length == 0){
					$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
				}else{
				jQuery.each(result, function(index, item) {					
					pageCount = item.pageCount;
					rowCount = item.rowCount;
					var monto = item.monto;
					var monto_cobrado = item.monto_cobrado;
					var por_cobrar = item.por_cobrar;
					var deuda_total = item.deuda_total;
					
					if(item.monto==null){
						monto = 0.00;
					}
					if(item.monto_cobrado==null){
						monto_cobrado = 0.00;
					}
					if(item.por_cobrar==null){
						por_cobrar = 0.00;
					}
					if(item.deuda_total==null){
						deuda_total = 0.00;
					}
		
            		contenido += '<tr>'+
            			 '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
            		   '<th class="font-weight-normal mb-0">'+item.zona+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.cantidad_parqueos+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.tiempo+'</th>'+
				       '<th class="font-weight-normal mb-0">'+moneda+Number.parseFloat(monto).toFixed(2)+'</th>'+
				       '<th class="font-weight-normal mb-0">'+moneda+Number.parseFloat(monto_cobrado).toFixed(2)+'</th>'+
				       '<th class="font-weight-normal mb-0">'+moneda+Number.parseFloat(por_cobrar).toFixed(2)+'</th>'+
				       '<th style="border-right: none;" class="font-weight-normal mb-0">'+moneda+Number.parseFloat(deuda_total).toFixed(2)+'</th>'+
				    '</tr>';	 	    
			        
        		});
        		if(page<=pageCount){
	        		var temp2 = '';
	        		for(var i = 1; i<=pageCount; i++){
						if(page == i){
							temp2 += '<li class="page-item active"> <a onclick="parqueosZona('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
						}else{
							temp2 += '<li class="page-item"> <a onclick="parqueosZona('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
						}
						
					}
	        		
	        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="parqueosZona('+(page-1)+')"'+
	        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
	        		'<li class="page-item"> <a class="page-link" onclick="parqueosZona('+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
	        		
	        		
	        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';
	        		
	        			        		
	        		$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
        		}
        		}
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
	}
}
/*
function mostrarDetalleIncidencia(id){	
var salida = '<form class="forms-sample" action="" accept-charset="UTF-8" >';	                  			                  
	$.ajax({
		contentType: 'application/json',
		    url: '/parqueo/incidenciaDetalleJSON?id='+id,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
					var date = new Date(result.inci_FechaCreacion);
					var fecha = date.getDate() + '/' + ( date.getMonth() + 1 ) + '/' + date.getFullYear();
					var hora = date.getHours() + ':' + date.getMinutes();
					var fechaYHora = fecha + ' ' + hora;
				
				salida = salida +'<div class="form-group">'+
		                      ' <label for="exampleInputFecha">Fecha y hora</label>'+
		                      '<input type="text" value="'+fechaYHora+'" class="form-control" id="exampleInputFecha" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      ' <label for="exampleInputTipo">Tipo de incidencia</label>  '+
		                      '<input type="text" value="'+result.tinc_Nombre+'" class="form-control" id="exampleInputTipo" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputComentarios">Descripción</label>  '+
		                      '<textarea name="textarea" class="form-control" rows="10" cols="50" readonly>'+result.inci_Descripcion+'</textarea>'+
		                    '</div>'+
		                    ' <p class="card-description" style="font-size: 14px; color: #010101;">Fotos</p>'+
		                    '<ul class="list-group"><div class="form-group">';
								
					$.ajax({
						contentType: 'application/json',
					    url: '/parqueo/clienteVehiculosJSON?id='+id,
					    type: 'GET',
					    dataType: 'json',
					    success: function(result2) {
							var cantidadImagenes = 0;
							
							jQuery.each(result2, function(index, item) {
								
			            		salida += '<img src="'+item.ifot_NombreArchivo+'" alt="..." class="img-thumbnail"><br><br>';
			            		cantidadImagenes++;  
			        		});
			        		
			        		if(cantidadImagenes==0){
								salida+='<p class="card-description" style="font-size: 15px;">No se adjuntaron imágenes</p>';
							}
			        		
			        		salida+= "</div></ul></form>";
			        		$(".modal-body").html(salida);
						},  error: function (jqXHR, textStatus, errorThrown) { 
			
					 	}});

			},  error: function (jqXHR, textStatus, errorThrown) { 

		 	}
	});
	
}*/
