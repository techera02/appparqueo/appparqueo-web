
$(".ejemplo3").change(function(){
            alert($('select[class=ejemplo3]').val());
            $('.valor3').val($(this).val());
	});
	
function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}

function eliminarZona(id, page){
	  Swal.fire({
		 title: '¿Quieres eliminar este zona?',
		  text: "Se eliminará permanentemente!",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, elimínalo'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/configuracion/eliminarZona?id='+id,
		    type: 'GET',
		    success: function(result) {
				if(!result){
					listaZonas(page, false);
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha eliminado correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
				
		    }
			});
		  }
		})
}

$(document).ready(function() {
    listaZonas(1, true);
    
    $("#textBoton").html('Filtrar');

});

function filtrarDatosEnter(){
	
	tecla = event.keyCode;
	
	if(tecla == 13){      
       listaZonas(1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;

    
function listaZonas(page, reiniciar){
	 var contenido = '';
	 var contenidoPaginador = '';
	 var contenidoTotalRegistros = '';
	 var pageCount = 1;
	 var rowCount = 0;
	 	 
	if(reiniciar == true){		
		var activo = 0;
		var idmuni = 0;
				
		$("#activo").val('0');
		$("#idperfil").val('0');
	}else{
		var idmuni = document.getElementById('idmuni').value;
		var activo = document.getElementById('activo').value;
	} 	 
	 
	if(page >=1 ){
	 $.ajax({
		contentType: 'application/json',
		    url: '/configuracion/zonasJSON?idmunicipio='+idmuni+'&page='+page+'&activo='+activo,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
			
				jQuery.each(result, function(index, item) {
	
					var temp = '';
					pageCount = item.pageCount;
					rowCount = item.rowCount;
					
					if(item.zona_Activo == true){
						var temp = 'SI';
					}else if (item.zona_Activo == false){
						var temp = 'NO';
					}
				
					
            		contenido += '<tr>'+
            		 '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
            		   '<th class="font-weight-normal mb-0">'+item.muni_Nombre+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.zona_Descripcion+'</th>'+
				       '<th class="font-weight-normal mb-0">'+temp+'</th>'+
				       '<th style="border-right: none;">'+
				       '<a href="/configuracion/estacionamiento/'+item.zona_Id+'" class="btn btn-primary" style="padding: 0.6rem 0.77rem;">Estacionamientos</a>'+
				       '<a href="/configuracion/zonaRegistro/'+item.zona_Id+'" class="btn btn-warning" style="padding: 0.6rem 0.77rem;">Editar</a>'+
				       '<button onclick="eliminarZona('+item.zona_Id+', '+page+')" class="btn btn-danger" style="padding: 0.6rem 0.77rem;">Eliminar</button></th>'+
				    '</tr>';	 	    
        		});
        		if(page<=pageCount){
	        		var temp2 = '';
	        		for(var i = 1; i<=pageCount; i++){
						if(page == i){
							temp2 += '<li class="page-item active"> <a onclick="listaZonas('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
						}else{
							temp2 += '<li class="page-item"> <a onclick="listaZonas('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
						}
						
					}
	        		
	        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="listaZonas('+(page-1)+')"'+
	        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
	        		'<li class="page-item"> <a class="page-link" onclick="listaZonas('+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
	        		
	        		
	        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';

	        		
	        			        		
	        		$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
        		}
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
}
