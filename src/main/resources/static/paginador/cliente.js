
$(document).ready(function() {
    listaClientes(1,true);
    $("#textBoton").html('Filtrar');
});

function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}
function filtrarDatosEnter(){
	
	tecla = event.keyCode;
	
	if(tecla == 13){      
       listaClientes(1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;

function quitarAcentos(cadena){
	const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
	return cadena.split('').map( letra => acentos[letra] || letra).join('').toString();	
}
function listaClientes(page,reiniciar){
	 var contenido = '';
	 var contenidoPaginador = '';
	 var contenidoTotalRegistros = '';
	 var pageCount = 0;
	 var rowCount = 0;
	 var indicadorNombre = 1;
	 var indicadorNrodocumento = 1;
	 var indicadorPlaca = 1;
	 
	 if(reiniciar == true){
		var nombre = null;
		var nrodocumento = null;
		var placa = null;
		
		indicadorNombre = 0;
		indicadorNrodocumento = 0;
		indicadorPlaca = 0;
		
		document.getElementById("nombre").value = "";
		document.getElementById("nrodocumento").value = "";
		document.getElementById("placa").value = "";
	
	}else{
		var nombre = document.getElementById('nombre').value;
		var nrodocumento = document.getElementById('nrodocumento').value;
		var placa = document.getElementById('placa').value;

		nombre = quitarAcentos(nombre);
		placa = quitarAcentos(placa);

		if(!nombre){
			nombre = null;
			indicadorNombre = 0;
		}
		if(!nrodocumento){
			nrodocumento = null;
			indicadorNrodocumento = 0;
		}
		if(!placa){
			placa = null;
			indicadorPlaca = 0;
		}
	} 	 
	if(page >=1 ){
		var moneda = document.getElementById('sesion_modena').value;
	 $.ajax({
		contentType: 'application/json',
		    url: '/maestros/clientesJSON?page='+page+'&nombre='+nombre+'&indiceNombre='+indicadorNombre+
		    '&nrodocumento='+nrodocumento+'&indiceNrodocumento='+indicadorNrodocumento+'&placa='+placa+
		    '&indicePlaca='+indicadorPlaca,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
				if(result.length == 0){
					$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
				}else{
				jQuery.each(result, function(index, item) {
					var temp = '';
					pageCount = item.pageCount;
					rowCount = item.rowCount;
					
            		contenido += '<tr>'+
            		 '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.clie_Nombres+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.clie_ApePaterno+' '+''+item.clie_ApeMaterno+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.tdoc_Nombre+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.clie_NroDocumento+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.vehi_Placa+'</th>'+
				       '<th class="font-weight-normal mb-0"><span style="float:right;">'+moneda+Number.parseFloat(item.saldo).toFixed(2)+'</span></th>'+
				       '<th style="border-right: none;"> <button onclick="mostrarDetalleCliente('+item.clie_Id+')" data-toggle="modal" data-target="#exampleModalLong" class="btn btn-primary" style="padding: 0.6rem 0.77rem;">Detalle</button></th>'+
				    '</tr>';	 	   
        		});
        		
        		if(page<=pageCount){
	        		var temp2 = '';
	        		for(var i = 1; i<=pageCount; i++){
						if(page == i){
							temp2 += '<li class="page-item active"> <a onclick="listaClientes('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
						}else{
							temp2 += '<li class="page-item"> <a onclick="listaClientes('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
						}
						
					}
	        		
	        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="listaClientes('+(page-1)+')"'+
	        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
	        		'<li class="page-item"> <a class="page-link" onclick="listaClientes('+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
	        		
	        		
	        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';
	        		
	        			        		
	        		$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
        		}
        		}
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
}

function mostrarDetalleCliente(id){
	var moneda = document.getElementById('sesion_modena').value;
	var salida = '<form class="forms-sample" action="" accept-charset="UTF-8" >';	                  			                  
	$.ajax({
		contentType: 'application/json',
		    url: '/parqueo/clienteDetalleJSON?id='+id,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
				var nombreDocumento = '';
				var nroDocumento = '';
				var saldoActual = 0;
				
				if(result.tdoc_Nombre != null){
					nombreDocumento = '' + result.tdoc_Nombre;
				}
				if(result.clie_NroDocumento != null){
					nroDocumento = '' + result.clie_NroDocumento;
				}
				if(result.saldo != null){
					saldoActual = result.saldo;
				}
				
				salida = salida +'<div class="form-group">'+
		                      '<label for="exampleInputNombres">Nombres</label>'+
		                      '<input type="text" value="'+result.clie_Nombres+'" class="form-control" id="exampleInputNombres" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputPaterno">Apellido Paterno</label>'+
		                      '<input type="text" value="'+result.clie_ApePaterno+'" class="form-control" id="exampleInputPaterno" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputMaterno">Apellido Materno</label>'+
		                      '<input type="text" value="'+result.clie_ApeMaterno+'" class="form-control" id="exampleInputMaterno" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputCorreo">Correo electrónico</label>'+
		                      '<input type="text" value="'+result.clie_Correo+'" class="form-control" id="exampleInputCorreo" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputNumero">Nro Celular</label>'+
		                      '<input type="text" value="'+result.clie_NroTelefono+'" class="form-control" id="exampleInputNumero" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputTipoDocumento">Tipo Documento</label>'+
		                      '<input type="text" value="'+nombreDocumento+'" class="form-control" id="exampleInputTipoDocumento" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group">'+
		                      '<label for="exampleInputDocumento">Nro Documento</label>'+
		                      '<input type="text" value="'+nroDocumento+'" class="form-control" id="exampleInputDocumento" placeholder="" readonly>'+
		                    '</div>'+
		                    '<div class="form-group"><label for="exampleInputSaldo" >Saldo Actual</label><div class="input-group">'+
		                      '<div class="input-group-prepend"><span class="input-group-text" style="color:GRAY;">'+moneda+'</span></div>'+
		                      '<input type="text" value="'+Number.parseFloat(saldoActual).toFixed(2)+'" class="form-control" id="exampleInputSaldo" placeholder="" readonly>'+
		                    '</div></div>'+
		                    ' <p class="card-description" style="font-size: 14px;color: #010101;">Vehículos</p>'+
		                    '<ul class="list-group">';
								
					$.ajax({
						contentType: 'application/json',
					    url: '/parqueo/clienteVehiculosJSON?id='+id,
					    type: 'GET',
					    dataType: 'json',
					    success: function(result2) {
							jQuery.each(result2, function(index, item) {
								
			            		salida += '<input type="text" class="form-control" value="'+item.vehi_Placa+'" placeholder="" readonly><br>';
			            		  	   
			        		});
			        		salida+= "</ul></form>";
			        		$(".modal-body").html(salida);
						},  error: function (jqXHR, textStatus, errorThrown) { 
			
					 	}});

			},  error: function (jqXHR, textStatus, errorThrown) { 

		 	}
	});
	
}
