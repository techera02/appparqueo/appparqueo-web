function eliminarEstacionamiento(id, page, idZona){
	  Swal.fire({
		  title: '¿Quieres eliminar este estacionamiento?',
		  text: "Se eliminará permanentemente!",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, elimínalo'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/configuracion/eliminarEstacionamiento?id='+id,
		    type: 'GET',
		    success: function(result) {
				
				if(!result){
					listaEstacionamientos(idZona, page,false);
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha eliminado correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
		    }
			});
		  }
		})
}

$(document).ready(function() {
	var idZona = document.getElementById('idZona').value;
    listaEstacionamientos(idZona, 1,true);
    $("#textBoton").html('Filtrar');
});
function filtrarDatosEnter(){
	var idZona = document.getElementById('idZona').value;
	tecla = event.keyCode;
	
	if(tecla == 13){      
       listaEstacionamientos(idZona,1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;


function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}

function quitarAcentos(cadena){
	const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
	return cadena.split('').map( letra => acentos[letra] || letra).join('').toString();	
}
function listaEstacionamientos(idZona, page,reiniciar){
	 var contenido = '';
	 var contenidoPaginador = '';
	 var pageCount = 1;
	 var rowCount = 0;
	 var contenidoTotalRegistros = '';
	 var indicadorNombre = 1;
	 var indicadorCodigo = 1;
	 
	 if(reiniciar == true){
		var nombre = null;
		indicadorNombre = 0;
		
		var codigo = null;
		indicadorCodigo = 0;
		
		document.getElementById("nombre").value = "";
		document.getElementById("codigo").value = "";

	}else{
		var nombre = document.getElementById('nombre').value;
		var codigo = document.getElementById('codigo').value;
		nombre = quitarAcentos(nombre);
		codigo = quitarAcentos(codigo);
		
		if(!nombre){
			nombre = null;
			indicadorNombre = 0;
		}
		
		if(!codigo){
			codigo = null;
			indicadorCodigo = 0;
		}
	} 	 
	if(page >=1 ){
		
	 $.ajax({
		contentType: 'application/json',
		    url: '/configuracion/estacionamientosJSON?idZona='+idZona+'&page='+page+'&nombre='+nombre
		    +'&indicenombre='+indicadorNombre+'&codigo='+codigo+'&indicecodigo='+indicadorCodigo,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
				if(result.length == 0){
					$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
				}else{
					
				
				jQuery.each(result, function(index, item) {
					var temp = '';
					var temp2 = '';
					pageCount = item.pageCount;
					rowCount = item.rowCount;
	
					if(item.esta_Disponible == true){
						var temp = 'SI';
					}else if (item.esta_Disponible == false){
						var temp = 'NO';
					}
					
					if(item.esta_Discapacitado == true){
						var temp2 = 'SI';
					}else if (item.esta_Discapacitado == false){
						var temp2 = 'NO';
					}
					
            		contenido += '<tr>'+
            		 '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.esta_Nombre+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.esta_Codigo+'</th>'+
				       '<th class="font-weight-normal mb-0">'+temp2+'</th>'+
				       '<th class="font-weight-normal mb-0">'+temp+'</th>'+
				       '<th style="border-right: none;"> <a href="/configuracion/estacionamientoRegistros/'+item.esta_Id+'" class="btn btn-warning" style="padding: 0.6rem 0.77rem;">Editar</a>'+
				       '<button onclick="eliminarEstacionamiento('+item.esta_Id+', '+page+', '+idZona+')" class="btn btn-danger" style="padding: 0.6rem 0.77rem;">Eliminar</button>'+
				    '</tr>';	 	    
        		});
        		
        		if(page<=pageCount){
				
	        		var temp2 = '';
	        		for(var i = 1; i<=pageCount; i++){
						if(page == i){
							temp2 += '<li class="page-item active"> <a onclick="listaEstacionamientos('+idZona+','+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
						}else{
							temp2 += '<li class="page-item"> <a onclick="listaEstacionamientos('+idZona+','+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
						}
						
					}
	        		
	        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="listaEstacionamientos('+idZona+','+(page-1)+')"'+
	        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
	        		'<li class="page-item"> <a class="page-link" onclick="listaEstacionamientos('+idZona+','+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
	        		
	        		
	        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';
	        		
	        			        		
	        		$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
        		}
        		}
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
}
