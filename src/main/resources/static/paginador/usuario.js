/*function eliminarUsuario(id, page){
	
	  Swal.fire({
		  title: '¿Quieres eliminar este usuario?',
		  text: "Se eliminará permanentemente!",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, elimínalo'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/seguridad/eliminarUsuario?id='+id,
		    type: 'GET',
		    success: function(result) {
				
				if(!result){
					listaUsuarios(page, false);
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha eliminado correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
		    }
			});
		  }
		})
}
*/

function quitarAcentos(cadena){
	const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
	return cadena.split('').map( letra => acentos[letra] || letra).join('').toString();	
}

$(document).ready(function() {
    listaUsuarios(1, true);    
    $("#textBoton").html('Filtrar');
              
});

function filtrarDatosEnter(){
	
	tecla = event.keyCode;
	
	if(tecla == 13){      
       listaUsuarios(1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;

function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}
   
function listaUsuarios(page, reiniciar){

	 var contenido = '';
	 var contenidoPaginador = '';
	 var contenidoTotalRegistros = '';
	 var pageCount = 0;
	 var rowCount = 0;
	 var indicadorNombre = 1;
	 	 
	if(reiniciar == true){
		var nombre = null;
		indicadorNombre = 0;
		
		var activo = 0;
		var idperfil = 0;
		
		document.getElementById("nombre").value = "";
		
		$("#activo").val('0');
		$("#idperfil").val('0');
	}else{
		var nombre = document.getElementById('nombre').value;
		var activo = document.getElementById('activo').value;
		var idperfil = document.getElementById('idperfil').value;
	
		nombre = quitarAcentos(nombre);
		 
		if(!nombre){
			nombre = null;
			indicadorNombre = 0;
		}
		if(!activo){
			activo = 0;
		}
		if(!idperfil){
			idperfil = 0;
		}
	} 	 
	

	if(page >=1 ){
		
	 $.ajax({
		contentType: 'application/json',
		    url: '/seguridad/usuariosFiltrosJSON?page='+page+'&nombre='+nombre+'&idperfil='+idperfil+
		    '&activo='+activo+'&indicadorNombre='+indicadorNombre,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
				
				if(result.length == 0){
					$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
				}else{
				
					jQuery.each(result, function(index, item) {
						var temp = '';
						pageCount = item.pageCount;
						rowCount = item.rowCount;
	
						if(item.usua_Activo == true){
							var temp = 'SI';
						}else if (item.usua_Activo == false){
							var temp = 'NO';
						}
						
	            		contenido += '<tr>'+
	            		    '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
					       '<th class="font-weight-normal mb-0">'+item.usua_Alias+'</th>'+
					       '<th class="font-weight-normal mb-0">'+item.usua_Nombres+'</th>'+
					       '<th class="font-weight-normal mb-0">'+item.usua_Apellidos+'</th>'+
					       '<th class="font-weight-normal mb-0">'+item.usua_Correo+'</th>'+
					       '<th class="font-weight-normal mb-0">'+item.perf_Nombre+'</th>'+
					       '<th class="font-weight-normal mb-0">'+temp+'</th>'+
					       '<th  style="border-right: none;"> <a href="/seguridad/usuarioRegistro/'+item.usua_Id+'" class="btn btn-warning" style="padding: 0.6rem 0.77rem;">Editar</a>'+
					       '<button onclick="reenviarCredencialesUsuario('+item.usua_Id+')" class="btn btn-info" style="padding: 0.6rem 0.77rem;" title="Reenviar"><i class="ti-email"></i></button></th>'+
					    '</tr>';	 	
					    
	        		});
	        		if(page<=pageCount){
		        		var temp2 = '';
		        		for(var i = 1; i<=pageCount; i++){
							if(page == i){
								temp2 += '<li class="page-item active"> <a onclick="listaUsuarios('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
							}else{
								temp2 += '<li class="page-item"> <a onclick="listaUsuarios('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
							}
							
						}
		        		
		        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="listaUsuarios('+(page-1)+')"'+
		        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
		        		'<li class="page-item"> <a class="page-link" onclick="listaUsuarios('+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
		        		
		        		
		        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';
		        		
		        			        		
		        		$(".cuerpo-tabla").html(contenido);
		        		$(".pagination").html(contenidoPaginador);
		        		$("#cuerpo-total").html(contenidoTotalRegistros);
		        		
	        		}
        		}
        		
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
}


function reenviarCredencialesUsuario(id){
	  Swal.fire({
		  title: '¿Quieres reenviar las credenciales al usuario?',
		  text: "Se reenviará las credenciales al correo del usuario.",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, reenviar'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/seguridad/ReenviarCredencialesUsuario?id='+id,
		    type: 'GET',
		    success: function(result) {
				
				if(!result){
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha reenviado las credenciales correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
		    }
			});
		  }
		})
}

