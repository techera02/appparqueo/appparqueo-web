/*function eliminarSupervisor(id, page){
	  Swal.fire({
		  title: '¿Quieres eliminar este supervisor?',
		  text: "Se eliminará permanentemente!",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, elimínalo'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/configuracion/eliminarSupervisor?id='+id,
		    type: 'GET',
		    success: function(result) {
				
				if(!result){
					listarSupervisores(page);
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha eliminado correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
		    }
			});
		  }
		})
}*/

$(document).ready(function() {
    listarSupervisores(1, true);
    $("#textBoton").html('Filtrar');
});

function filtrarDatosEnter(){
	
	tecla = event.keyCode;
	
	if(tecla == 13){      
       listarSupervisores(1,false);    
    }  
}

window.onkeydown = filtrarDatosEnter;


function eventBoton(){
	$("button[aria-expanded='false']").prop( $("#textBoton").html('Ocultar'));
	$("button[aria-expanded='true']").prop( $("#textBoton").html('Filtrar'));
}


function quitarAcentos(cadena){
	const acentos = {'á':'a','é':'e','í':'i','ó':'o','ú':'u','Á':'A','É':'E','Í':'I','Ó':'O','Ú':'U'};
	return cadena.split('').map( letra => acentos[letra] || letra).join('').toString();	
}

function listarSupervisores(page, reiniciar){
	 var contenido = '';
	 var contenidoPaginador = '';
	 var pageCount = 0;
	 var rowCount = 0;
	 
	 var nombre = document.getElementById('nombre').value;
	 var documento = document.getElementById('documento').value;
	 var activo = document.getElementById('activo').value;
	 
	 nombre = quitarAcentos(nombre);

	 
	 if(reiniciar == true){
				
		document.getElementById("nombre").value = "";
		document.getElementById("documento").value = "";
		$("#activo").val('0');
		
		nombre = '*';
		documento = '*';
		activo = 0;
		
	}else{

		if(nombre == ""){
		nombre = '*';
		}
		
		if(documento == ""){
		documento = '*';
		}
	}
	 
	if(page >=1 ){
		
	 $.ajax({
		contentType: 'application/json',
		    url: '/configuracion/supervisoresJSON?page='+page+'&nombre='+nombre+'&documento='+documento+'&activo='+activo,
		    type: 'GET',
		    dataType: 'json',
		    success: function(result) {
			
				if(result.length == 0){
					$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
				}else{
			
				jQuery.each(result, function(index, item) {
					var temp = '';
					pageCount = item.pageCount;
					rowCount = item.rowCount;

					if(item.supe_Activo == true){
						var temp = 'SI';
					}else if (item.supe_Activo == false){
						var temp = 'NO';
					}
					
            		contenido += '<tr>'+
            		   '<th class="font-weight-normal mb-0">'+item.rowNumber+'</th>'+
            		   '<th class="font-weight-normal mb-0">'+item.supe_Alias+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.supe_Nombres+'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.supe_ApellidoPaterno + ' ' + item.supe_ApellidoMaterno +'</th>'+
				       '<th class="font-weight-normal mb-0">'+item.supe_NroDocumento+'</th>'+
				        '<th class="font-weight-normal mb-0">'+item.supe_Correo+'</th>'+
				       '<th class="font-weight-normal mb-0">'+temp+'</th>'+
				       '<th style="border-right: none;"> <a href="/configuracion/supervisorRegistro/'+item.supe_Id+'" class="btn btn-warning" style="padding: 0.6rem 0.77rem;">Editar</a>'+
				       '<button onclick="reenviarCredencialesSupervisor('+item.supe_Id+')" class="btn btn-info" style="padding: 0.6rem 0.77rem;" title="Reenviar"><i class="ti-email"></i></button></th>'+
				    '</tr>';	 	    
        		});
        		if(page<=pageCount){
	        		var temp2 = '';
	        		for(var i = 1; i<=pageCount; i++){
						if(page == i){
							temp2 += '<li class="page-item active"> <a onclick="listarSupervisores('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';
						}else{
							temp2 += '<li class="page-item"> <a onclick="listarSupervisores('+i+')" style="cursor: pointer;" class="page-link">'+i+'</a></li>';	
						}
						
					}
	        		
	        		contenidoPaginador = '<li class="page-item"> <a class="page-link" onclick="listarSupervisores('+(page-1)+')"'+
	        		'tabindex="-1" style="cursor: pointer;">Anterior</a> </li>'+temp2+
	        		'<li class="page-item"> <a class="page-link" onclick="listarSupervisores('+(page+1)+')" style="cursor: pointer;">Siguiente</a> </li>';
	        		
	        		contenidoTotalRegistros = '<p class="card-description">'+rowCount+' registros</p>';
	        		
	        		$(".cuerpo-tabla").html(contenido);
	        		$(".pagination").html(contenidoPaginador);
	        		$("#cuerpo-total").html(contenidoTotalRegistros);
        		}
        		}
		    },  error: function (jqXHR, textStatus, errorThrown) { 
				var errorMicroservicio = '<div class="alert alert-danger alert-dismissible fade show" role="alert" >'+
				'Este servicio no esta disponible por el momento'+
				'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
				'<span aria-hidden="true">&times;</span></button>'+
				'</div>';

				$(".cuerpo-tabla").html('');
				$(".pagination").html('');
				$(".error-microservice").html(errorMicroservicio);
		 	}
	});
	}
}

function reenviarCredencialesSupervisor(id){
	  Swal.fire({
		  title: '¿Quieres reenviar las credenciales al supervisor?',
		  text: "Se reenviará las credenciales al correo del supervisor.",
		  showCancelButton: true,
		  confirmButtonColor: '#59C3B7',
		  cancelButtonColor: '#d33',
		  cancelButtonText: 'Cancelar',
		  confirmButtonText: 'Si, reenviar'
		}).then((result) => {
		  if (result.isConfirmed) {
		    $.ajax({
		    url: '/configuracion/ReenviarCredencialesSupervisor?id='+id,
		    type: 'GET',
		    success: function(result) {
				
				if(!result){
					Swal.fire({
					  icon: 'success',
					  title: 'Se ha reenviado las credenciales correctamente',
					  showConfirmButton: false,
					  timer: 1500
					})
				}else{
					Swal.fire({
					  icon: 'error',
					  title: result,
					  confirmButtonColor: '#59C3B7',
					  showConfirmButton: true
					})
				}
		    }
			});
		  }
		})
}